package com.xilenda.core;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.utils.PreferenceUtils;

/**
 * Created by Israel MEKOMOU [15/07/2020].
 */
public class CustomerHelper {
    public static final String CURRENT_USER = "current_user";
    private static Customer currentCustomer;

    public static Customer getCurrentCustomer(Context context){
        Customer customer = null;
        String userString = PreferenceUtils.getString(context, CURRENT_USER);
        Log.e("USER", userString);
        if (currentCustomer != null){
            customer = CustomerHelper.currentCustomer;
        }else {
            if (!userString.equals("")){
                customer = new Gson().fromJson(new JsonParser().parse(userString), Customer.class);
                currentCustomer = customer;
            }
        }
        assert currentCustomer != null;
//        Log.e("CUSTOMER", currentCustomer.toString());
        return customer;
    }

    public static void setCurrentCustomer(Context context, Customer customer){
        if (customer != null){
            PreferenceUtils.setString(context, CURRENT_USER, new Gson().toJson(customer));
        }
        currentCustomer = customer;
    }
}
