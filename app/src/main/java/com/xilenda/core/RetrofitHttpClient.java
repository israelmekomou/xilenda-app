package com.xilenda.core;

import android.util.Log;

import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;


import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import me.gilo.woodroid.data.auth.AuthIntercepter;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class RetrofitHttpClient {
    private static Retrofit retrofit = null;
    private static Retrofit retrofitWC = null;
    public static final int API_CONNECTION_TIMEOUT = 30; //seconds
    public static final int API_WRITE_TIMEOUT = 30; //seconds
    public static final int API_READ_TIMEOUT = 60; //seconds

    public static Retrofit getClient(){
        if (retrofit == null){
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new AuthIntercepter(Utils.CONSUMER_KEY, Utils.CONSUMER_SECRETE))
                    .addInterceptor(loggingInterceptor)
                    .connectTimeout(API_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(API_WRITE_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(API_READ_TIMEOUT, TimeUnit.SECONDS)
                    .addNetworkInterceptor(new Interceptor() {
                        @NotNull
                        @Override
                        public Response intercept(@NotNull Chain chain) throws IOException {
                            Request.Builder builder = chain.request().newBuilder()
                                    .addHeader("Connection", "close");
                            return chain.proceed(builder.build());
                        }
                    })
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(ApiUtils.BASE_JSON_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    public static Retrofit getWCClient(){
        if (retrofitWC == null){
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new AuthIntercepter(Utils.CONSUMER_KEY, Utils.CONSUMER_SECRETE))
                    .addInterceptor(logging)
                    .connectTimeout(API_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(API_WRITE_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(API_READ_TIMEOUT, TimeUnit.SECONDS)
                    .addNetworkInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request.Builder builder = chain.request().newBuilder()
                                    .addHeader("Connection", "close");
                            return chain.proceed(builder.build());
                        }
                    }).build();

            retrofitWC = new Retrofit.Builder()
                    .baseUrl(ApiUtils.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofitWC;
    }
}
