package com.xilenda;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.xilenda.core.CustomerHelper;
import com.xilenda.data.DatabaseOpenHelper;
import com.xilenda.data.ProviderContract;
import com.xilenda.data.ProviderContract.Cart;
import com.xilenda.ui.authentication.AuthActivity;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.ui.authentication.login.LoginActivity;
import com.xilenda.ui.authentication.register.RegisterActivity;
import com.xilenda.ui.contact_us.ContactUsActivity;
import com.xilenda.ui.home.HomeFragment;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.PreferenceUtils;
import com.xilenda.utils.Utils;

import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class MainActivity extends BaseActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout mDrawer;
    private long mBackPressedTime;
    private Toast mBackToast;
    private NavController mNavController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar(this, getString(R.string.app_name), false, false, false);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWhatsapp(view, getString(R.string.contact_number), "");
            }
        });

        mDrawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        Customer customer = CustomerHelper.getCurrentCustomer(this);
        showNavItems(navigationView.getMenu(), customer != null);
        initCustomer(navigationView.getHeaderView(0), customer);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_order_history, R.id.nav_profile,
                R.id.nav_about_us, R.id.nav_contact_us, R.id.nav_logout,
                R.id.nav_share)
                .setDrawerLayout(mDrawer)
                .build();
        mNavController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, mNavController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, mNavController);
        handleCommunicationMenu(navigationView.getMenu());
    }

    private void handleCommunicationMenu(Menu navMenu) {
        navMenu.findItem(R.id.nav_about_us).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mDrawer.closeDrawers();
                return false;
            }
        });
        navMenu.findItem(R.id.nav_contact_us).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mDrawer.closeDrawers();
                Utilities.openActivity(MainActivity.this, ContactUsActivity.class.getName());
                return false;
            }
        });
        navMenu.findItem(R.id.nav_share).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mDrawer.closeDrawers();
                shareApp();
                return true;
            }
        });
        navMenu.findItem(R.id.nav_logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mDrawer.closeDrawers();
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                        .setIcon(R.drawable.ic_warning_24dp)
                        .setTitle(getString(R.string.title_warning))
                        .setMessage(R.string.logout_check_message)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                logout();
                            }
                        })
                        .setNeutralButton("Non", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                if (!isFinishing())builder.create().show();
                return true;
            }
        });
    }

    private void shareApp() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message));
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_app)));
        Utilities.showForwardTransition(this);
    }

    private void logout() {
        Intent intent = new Intent(MainActivity.this, AuthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        Utilities.showBackwardTransition(MainActivity.this);
        CustomerHelper.setCurrentCustomer(this, null);
        PreferenceUtils.clear(this);
        new DatabaseOpenHelper(this).deleteTable(Cart.TABLE_NAME);
    }

    private void initCustomer(View v, Customer customer){
        LinearLayout layoutGuest = v.findViewById(R.id.lytGuest);
        LinearLayout layoutCustomer = v.findViewById(R.id.lytCustomer);
        TextView textViewLogin = v.findViewById(R.id.tvLogin);
        TextView textViewSignIn = v.findViewById(R.id.tvSignIn);
        TextView textViewUsername = v.findViewById(R.id.tvUsername);
        TextView textViewEmail = v.findViewById(R.id.tvEmail);
        TextView textViewJoin = v.findViewById(R.id.tvJoin);
        layoutCustomer.setVisibility( customer == null ? View.GONE : View.VISIBLE);
        textViewJoin.setVisibility(customer == null ? View.VISIBLE : View.GONE);
//        layoutGuest.setVisibility( customer == null ? View.VISIBLE : View.GONE);
        textViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.openActivity(MainActivity.this, LoginActivity.class.getName());
                finish();
            }
        });
        textViewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.openActivity(MainActivity.this, RegisterActivity.class.getName());
                finish();
            }
        });
        textViewJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.openActivity(MainActivity.this, AuthActivity.class.getName());
                finish();
            }
        });
        if (customer == null)return;
        textViewUsername.setText(customer.getFirst_name());
        textViewEmail.setText(customer.getEmail());

    }

    private void showNavItems(Menu navMenu, boolean show){
        navMenu.findItem(R.id.nav_order_history).setVisible(show);
        navMenu.findItem(R.id.nav_profile).setVisible(show);
        navMenu.findItem(R.id.nav_logout).setVisible(show);
        navMenu.findItem(R.id.nav_about_us).setVisible(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartRows();
    }

    @Override
    protected void onDestroy() {
        PreferenceUtils.setString(this, HomeFragment.CATEGORIES, "");
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        if (navHostFragment == null)return;
        if (navHostFragment.getChildFragmentManager().getFragments().get(0) instanceof HomeFragment){
            if (mDrawer.isDrawerOpen(GravityCompat.START)){
                mDrawer.closeDrawers();
            }else {
                if (mBackPressedTime + 3000 > System.currentTimeMillis()){
                    mBackToast.cancel();
                    super.onBackPressed();
                }else {
                    mBackToast = Toast.makeText(this, R.string.clic_twice_to_exit, Toast.LENGTH_LONG);
                    mBackToast.show();
                }
                mBackPressedTime = System.currentTimeMillis();
            }
        }else {
            mNavController.navigateUp();
        }
    }
}