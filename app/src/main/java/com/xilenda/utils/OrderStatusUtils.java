package com.xilenda.utils;

import com.xilenda.R;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class OrderStatusUtils {
    public static final String PENDING_PAYEMENT = "En attente";
    public static final String FAILED = "Echouée";
    public static final String PROCESSING = "En cours";
    public static final String COMPLETED = "Livrée";
    public static final String ON_HOLD = "En attente";
    public static final String CANCELLED = "Annulée";
    public static final String REFUNDED = "Remboursée";
    public static final String AUTHENTICATION_REQUIRED = "En attente d'authentification";
    public static final String PENDING_STATUS = "pending";
    public static final String ON_HOLD_STATUS = "on-hold";
    public static final String PROCESSING_STATUS = "processing";
    public static final String COMPLETED_STATUS = "completed";
    public static final String CANCELLED_STATUS = "cancelled";
    public static final String FAILED_STATUS = "failed";
    public static final String REFUNDED_STATUS = "refunded";

    public static int getStatusColor(String status){
        switch (status){
            case PENDING_STATUS :
            case ON_HOLD_STATUS :
            case PROCESSING_STATUS :
                return R.color.grey_60;
            case FAILED_STATUS :
            case CANCELLED_STATUS :
                return R.color.error_red;
            case COMPLETED_STATUS :
                return R.color.whatsappGreen;
            default:
                return R.color.colorPrimary;
        }
    }

    public static String getHumanStatus(String status){
        switch (status){
            case PENDING_STATUS:
            case ON_HOLD_STATUS:
            default:
                return ON_HOLD;
            case PROCESSING_STATUS:
                return PROCESSING;
            case COMPLETED_STATUS:
                return COMPLETED;
            case CANCELLED_STATUS:
                return CANCELLED;
            case FAILED_STATUS:
                return FAILED;
            case REFUNDED_STATUS:
                return REFUNDED;
        }
    }
}
