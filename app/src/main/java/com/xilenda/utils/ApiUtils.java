package com.xilenda.utils;


import com.xilenda.apiServices.AuthenticationApi;
import com.xilenda.apiServices.OrderApi;
import com.xilenda.apiServices.ProductsApi;
import com.xilenda.core.RetrofitHttpClient;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class ApiUtils {
    public static final String BASE_JSON_URL = "https://xilenda.com/wp-json/";
    public static final String BASE_URL = "https://xilenda.com";

    private ApiUtils(){}

    public static ProductsApi getProductsService(){
        return RetrofitHttpClient.getWCClient().create(ProductsApi.class);
    }

    public static ProductsApi getCProductsService(){
        return RetrofitHttpClient.getClient().create(ProductsApi.class);
    }

    public static AuthenticationApi getAuthenticationService(){
        return RetrofitHttpClient.getClient().create(AuthenticationApi.class);
    }

    public static OrderApi getOrderApi(){
        return RetrofitHttpClient.getClient().create(OrderApi.class);
    }
}
