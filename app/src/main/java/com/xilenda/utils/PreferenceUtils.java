package com.xilenda.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class PreferenceUtils {
    public static final String PREF_NAME = "XilendaPreferences";

    private PreferenceUtils(){}

    public static SharedPreferences getPrefs(Context context){
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String getString(Context context, String key){
        return getPrefs(context).getString(key, "");
    }

    public static int getInt(Context context, String key){
        return getPrefs(context).getInt(key, 0);
    }

    public boolean getBoolean(Context context, String key){
        return getPrefs(context).getBoolean(key, false);
    }

    public static void setString (Context context, String key, String value){
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setInt (Context context, String key, int value){
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void setBoolean (Context context, String key, boolean value){
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void clear(Context context){
        getPrefs(context).edit().clear().apply();
    }
}
