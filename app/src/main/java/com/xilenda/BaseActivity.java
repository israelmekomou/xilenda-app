package com.xilenda;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import com.google.android.material.snackbar.Snackbar;
import com.xilenda.data.DatabaseContract;
import com.xilenda.data.ProviderContract;
import com.xilenda.ui.cart.Cart;
import com.xilenda.ui.cart.CartActivity;
import com.xilenda.ui.search.SearchActivity;
import com.xilenda.uiComponents.Utilities;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [08/07/2020].
 */
public class BaseActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks <Cursor> {

    private MutableLiveData<ArrayList<Cart>> mCarts;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    protected void initToolbar(final Activity activity, String title, boolean hideTitle, final boolean hideCart, boolean hideSearch){
        mCarts = new MutableLiveData<>();
        Toolbar toolbar = findViewById(R.id.toolbar);
        final ImageButton buttonSearch = findViewById(R.id.ibSearch);
        final ImageButton buttonCart = findViewById(R.id.ibCart);
        final TextView textViewTitle = findViewById(R.id.tvTitle);
        final RelativeLayout layoutCount = findViewById(R.id.rlCount);
        final TextView textViewCount = findViewById(R.id.tvCount);


        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
                Utilities.showBackwardTransition(activity);
            }
        });

        textViewTitle.setText(title);
        textViewTitle.setVisibility(hideTitle ? View.GONE : View.VISIBLE);
        buttonCart.setVisibility(hideCart ? View.GONE : View.VISIBLE);
        buttonSearch.setVisibility(hideSearch ? View.GONE : View.VISIBLE);

        buttonCart.setColorFilter(ContextCompat.getColor(this, R.color.grey_80));
        buttonSearch.setColorFilter(ContextCompat.getColor(this, R.color.grey_80));

        buttonCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.openActivity(activity, CartActivity.class.getName());
            }
        });

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.openActivity(activity, SearchActivity.class.getName());
            }
        });

        LoaderManager.getInstance(this).restartLoader(CartActivity.LOADER_CART, null, this);

        mCarts.observe(this, new Observer<ArrayList<Cart>>() {
            @Override
            public void onChanged(ArrayList<Cart> carts) {
                int size = carts.size();
                if (size > 0){
                    if(!hideCart)layoutCount.setVisibility(View.VISIBLE);
                }else {
                    layoutCount.setVisibility(View.GONE);
                }
                textViewCount.setText(String.valueOf(size));
            }
        });

    }

    public void getCartRows(){
        LoaderManager.getInstance(this).restartLoader(CartActivity.LOADER_CART, null, this);
    }

    public boolean isProductInCart(int id){
        boolean productInCart = false;
        if (mCarts.getValue() != null){
            for (Cart c : mCarts.getValue())
                if (id == c.getProduct_id()) {
                    productInCart = true;
                    break;
                }
        }
        return productInCart;
    }

    public int[] getSameProductId(int id){
        if (mCarts.getValue() != null){
            for (Cart c : mCarts.getValue())
                if (id == c.getProduct_id()){
                    return new int[]{c.getId(), c.getQuantity()};
                }
        }
        return null;
    }

    public void openWhatsapp(View view, String phoneNumber, String message){
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("https://api.whatsapp.com/send?phone=%s&text=%s",
                    phoneNumber, message))));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Snackbar.make(view, R.string.hint_install_whatsapp, Snackbar.LENGTH_LONG).show();
        }
    }

    public ArrayList<Cart> getCartItems(){
        return mCarts.getValue();
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        CursorLoader loader = null;
        if (id == CartActivity.LOADER_CART){
            loader = new CursorLoader(this, ProviderContract.Cart.CONTENT_URI, null, null, null, null);
        }
        assert loader != null;
        return loader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == CartActivity.LOADER_CART){
            assert data != null;
            ArrayList<Cart> carts = new ArrayList<>();
            data.moveToFirst();
            while (!data.isAfterLast()){
                Cart cart = new Cart();
                cart.setId(data.getInt(data.getColumnIndex(DatabaseContract.CartEntry._ID)));
                cart.setQuantity(data.getInt(data.getColumnIndex(DatabaseContract.CartEntry.COLUMN_QUANTITY)));
                cart.setMax_quantity(data.getInt(data.getColumnIndex(DatabaseContract.CartEntry.COLUMN_MAX_QUANTITY)));
                cart.setProduct_id(data.getInt(data.getColumnIndex(DatabaseContract.CartEntry.COLUMN_PRODUCT_ID)));
                cart.setPrice(data.getInt(data.getColumnIndex(DatabaseContract.CartEntry.COLUMN_PRICE)));
                cart.setName(data.getString(data.getColumnIndex(DatabaseContract.CartEntry.COLUMN_NAME)));
                cart.setImage(data.getString(data.getColumnIndex(DatabaseContract.CartEntry.COLUMN_IMAGE)));
                carts.add(cart);
                data.moveToNext();
            }
            mCarts.setValue(carts);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        if (loader.getId() == CartActivity.LOADER_CART)mCarts.setValue(null);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_right);
    }
}
