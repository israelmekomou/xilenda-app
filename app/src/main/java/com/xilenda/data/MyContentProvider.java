package com.xilenda.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.xilenda.data.ProviderContract.Cart;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Israel MEKOMOU [07/07/2020].
 */
public class MyContentProvider extends ContentProvider {
    public static final String MINE_VENDOR_TYPE = "vnd" + ProviderContract.AUTHORITY;
    public static final int CART = 0;
    public static final int CART_ROW = 1;
    public static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private DatabaseOpenHelper mOpenHelper;

    static {
        uriMatcher.addURI(ProviderContract.AUTHORITY, Cart.PATH, CART);
        uriMatcher.addURI(ProviderContract.AUTHORITY, Cart.PATH + "/#", CART_ROW);
    }

    public MyContentProvider() {
    }

    @Override
    public int delete(@NotNull Uri uri, String selection, String[] selectionArgs) {
        long rowId = -1;
        String rowSelection = null;
        String[] rowSelectionArgs = null;
        int nRows = -1;
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int uriMatch = uriMatcher.match(uri);
        switch (uriMatch){
            case CART :
                nRows = db.delete(Cart.TABLE_NAME, selection, selectionArgs);
                break;
            case CART_ROW :
                rowId = ContentUris.parseId(uri);
                rowSelection = Cart._ID + " = ?";
                rowSelectionArgs = new String[]{Long.toString(rowId)};
                nRows = db.delete(Cart.TABLE_NAME, rowSelection, rowSelectionArgs);
                break;
        }
        return nRows;
    }

    @Override
    public String getType(@NotNull Uri uri) {
       String mimeType = null;
       int uriMatch = uriMatcher.match(uri);
       switch (uriMatch){
           case CART :
               mimeType = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" +
                       MINE_VENDOR_TYPE + "." + Cart.PATH;
               break;
           case CART_ROW :
               mimeType = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" +
                       MINE_VENDOR_TYPE + "." + Cart.PATH;
               break;
       }
       return mimeType;
    }

    @Override
    public Uri insert(@NotNull Uri uri, ContentValues values) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = -1;
        Uri rowUri = null;
        int uriMatch = uriMatcher.match(uri);
        switch (uriMatch){
            case CART :
                rowId = db.insert(Cart.TABLE_NAME, null, values);
                rowUri = ContentUris.withAppendedId(Cart.CONTENT_URI, rowId);
                break;
        }
        return rowUri;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseOpenHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(@NotNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        int uriMatch = uriMatcher.match(uri);
        switch (uriMatch){
            case CART :
                cursor = db.query(Cart.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case CART_ROW :
                long rowId = ContentUris.parseId(uri);
                String rowSelection = Cart._ID + " = ?";
                String[] rowSelectionArgs = new String[]{Long.toString(rowId)};
                cursor = db.query(Cart.TABLE_NAME, projection, rowSelection, rowSelectionArgs,
                null, null, null);
                break;
        }
        return cursor;
    }

    @Override
    public int update(@NotNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = -1;
        int nRows = -1;
        String rowSelection = null;
        String[] rowSelectionArgs = null;
        int uriMatch = uriMatcher.match(uri);
        switch (uriMatch){
            case CART :
                nRows = db.update(Cart.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CART_ROW :
                rowId = ContentUris.parseId(uri);
                rowSelection = Cart._ID + " = ?";
                rowSelectionArgs = new String[]{Long.toString(rowId)};
                nRows = db.update(Cart.TABLE_NAME, values, rowSelection, rowSelectionArgs);
                break;
        }
        return nRows;
    }
}
