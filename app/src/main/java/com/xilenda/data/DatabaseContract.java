package com.xilenda.data;

import android.provider.BaseColumns;

/**
 * Created by Israel MEKOMOU [07/07/2020].
 */
public final class DatabaseContract {
    private DatabaseContract(){}

    public static final class CartEntry implements BaseColumns{
        public static final String TABLE_NAME = "cart";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_IMAGE = "image";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_QUANTITY = "quantity";
        public static final String COLUMN_MAX_QUANTITY = "max_quantity";
        public static final String COLUMN_PRODUCT_ID = "product_id";
        public static final String COLUMN_ATTRIBUTES = "attributes";

        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME + " TEXT NOT NULL, " +
                        COLUMN_IMAGE + " TEXT NOT NULL, " +
                        COLUMN_PRICE + " INTEGER NOT NULL, " +
                        COLUMN_QUANTITY + " INTEGER NOT NULL, " +
                        COLUMN_MAX_QUANTITY + " INTEGER, " +
                        COLUMN_PRODUCT_ID + " INTEGER NOT NULL, " +
                        COLUMN_ATTRIBUTES + " TEXT)";

        public static final String SQL_DELETE_TABLE = "DELETE FROM "+ TABLE_NAME;
    }
}
