package com.xilenda.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.xilenda.data.DatabaseContract.CartEntry;

/**
 * Created by Israel MEKOMOU [07/07/2020].
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Xilenda.db";
    public static final int DATABASE_VERSION = 1;

    public DatabaseOpenHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CartEntry.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void deleteTable(String tableName){
        try (SQLiteDatabase db_write = getWritableDatabase()) {
            db_write.execSQL("DELETE FROM "+tableName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
