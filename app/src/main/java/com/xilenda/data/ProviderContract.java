package com.xilenda.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Israel MEKOMOU [07/07/2020].
 */
public final class ProviderContract {
    public ProviderContract() {
    }

    public static final String AUTHORITY = "com.xilenda.provider";
    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    protected interface CartColumns{
        String TABLE_NAME = "cart";
        String COLUMN_NAME = "name";
        String COLUMN_IMAGE = "image";
        String COLUMN_PRICE = "price";
        String COLUMN_QUANTITY = "quantity";
        String COLUMN_MAX_QUANTITY = "max_quantity";
        String COLUMN_PRODUCT_ID = "product_id";
        String COLUMN_ATTRIBUTES = "attributes";
    }

    public static final class Cart implements CartColumns, BaseColumns{
        public static final String PATH = "cart";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, PATH);
    }
}
