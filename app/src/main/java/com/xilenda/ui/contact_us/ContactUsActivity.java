package com.xilenda.ui.contact_us;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.Utils;

/**
 * Created by Israel MEKOMOU [03/08/2020].
 */
public class ContactUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utilities.hideKeyBoard(this);
        initToolbar(this, getString(R.string.app_name), false, false, false);
    }

    private void initUi() {
        final TextInputEditText editTextTitle = findViewById(R.id.etTitle);
        final TextInputEditText editTextMessage = findViewById(R.id.etMessage);
        TextView textViewGreeting = findViewById(R.id.tvGreeting);
        FloatingActionButton buttonCall = findViewById(R.id.fabCall);
        FloatingActionButton buttonMessage = findViewById(R.id.fabMessage);
        Button button = findViewById(R.id.btnSend);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (verify(editTextTitle, editTextMessage)){
                    sendEmail(editTextTitle.getText().toString(), editTextMessage.getText().toString());
                }
            }
        });

        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall();
            }
        });

        buttonMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage(view);
            }
        });
        textViewGreeting.setText(R.string.hello);
        Customer customer = CustomerHelper.getCurrentCustomer(this);
        if (customer == null)return;
        textViewGreeting.setText(getString(R.string.hello).concat(" ").concat(customer.getFirst_name()));
    }

    private void sendEmail(String title, String message) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc2822");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ "israelmekomou@gmail.com", "xilenda237@gmail.com", "mezazemp@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, title);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(emailIntent,
                "Envoyer un mail avec..."));
    }

    private void sendMessage(View v){
        try {
            Uri uri = Uri.parse("smsto:" + Utils.PHONE);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
            Snackbar.make(v, R.string.install_sms_app, 7000).show();
        }
    }

    private void makeCall(){
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Utils.PHONE));
        startActivity(callIntent);
    }

    private boolean verify(TextInputEditText title, TextInputEditText message){
        boolean juge = true;
        if (TextUtils.isEmpty(title.getText())){
            title.setError(getString(R.string.field_is_mandatory));
            juge = false;
        }else {
            title.setError(null);
        }
        if (TextUtils.isEmpty(message.getText())){
            message.setError(getString(R.string.field_is_mandatory));
            juge = false;
        }else {
            message.setError(null);
        }
        return juge;
    }
}