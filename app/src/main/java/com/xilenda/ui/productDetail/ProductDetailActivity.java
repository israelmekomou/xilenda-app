package com.xilenda.ui.productDetail;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.data.DatabaseContract.CartEntry;
import com.xilenda.data.ProviderContract.Cart;
import com.xilenda.ui.cart.CartActivity;
import com.xilenda.ui.product.Product;
import com.xilenda.ui.product.ProductAdapter;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.Utils;

import es.dmoral.toasty.Toasty;

import static com.xilenda.data.ProviderContract.Cart.CONTENT_URI;

/**
 * Created by Israel MEKOMOU [11/07/2020].
 */
public class ProductDetailActivity extends BaseActivity{

    private Product mProduct;
    private Button mButtonAddToCart;
    private int mMaxQuantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        super.initToolbar(this, getString(R.string.app_name), false, false, false);
        verifyProductQuantityInCart();
    }

    private void verifyProductQuantityInCart() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int[] ids = getSameProductId(mProduct.getId());
                if (ids != null){
                    if (ids[1] >= mMaxQuantity){
                        Toasty.info(ProductDetailActivity.this, "Votre panier contient la quantité maximum de ce produit.").show();
                        mButtonAddToCart.setEnabled(false);
                    }else {
                        mButtonAddToCart.setEnabled(true);
                    }
                }
            }
        }, 500);
    }

    private void initUi() {
        mProduct = null;
        Intent intent = getIntent();
        ImageView imageViewProduct = findViewById(R.id.ivProduct);
        TextView textViewProductName = findViewById(R.id.tvProductName);
        TextView textViewProductPrice = findViewById(R.id.tvProductPrice);
        TextView textViewProductDescription = findViewById(R.id.tvDescription);
        final TextView textViewQuantity = findViewById(R.id.tvQuantity);
        mButtonAddToCart = findViewById(R.id.btnAddToCart);
        ImageButton buttonAdd = findViewById(R.id.ibAdd);
        ImageButton buttonRemove = findViewById(R.id.ibRemove);

        if (intent != null){
            mProduct = intent.getParcelableExtra(ProductAdapter.PRODUCT);
            assert mProduct != null;
            textViewProductName.setText(mProduct.getName());
            textViewProductPrice.setText(mProduct.getPrice().concat(Utils.CURRENCY));
            textViewProductDescription.setText(stripHtml(mProduct.getDescription()));
            if (mProduct.getImages() != null && mProduct.getImages().size() > 0)Utilities.displayImageFromUrl(this, mProduct.getImages().get(0).getSrc(), imageViewProduct);

            mButtonAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int[] ids = getSameProductId(mProduct.getId());
                    if (ids == null){
                        addToCart(Integer.parseInt(textViewQuantity.getText().toString()));
                    }else {
                        updateProductQuantity(ids[0], ids[1], Integer.parseInt(textViewQuantity.getText().toString()));
                    }
                    showAlertDialog();
                }
            });
            mMaxQuantity = mProduct.getStock_quantity() == null || mProduct.getStock_quantity().equals("0") ? 10 : Integer.parseInt(mProduct.getStock_quantity());
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int[] ids = getSameProductId(mProduct.getId());
                    int realMax = mMaxQuantity;
                    if (ids != null){
                        if (ids[1] < mMaxQuantity){
                            realMax = mMaxQuantity - ids[1];
                        }
                    }
                    int quantity = changeQuantity(Integer.parseInt(textViewQuantity.getText().toString()), realMax, true);
                    if (quantity == 0)return;

                    textViewQuantity.setText(String.valueOf(quantity));
                }
            });

            buttonRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = changeQuantity(Integer.parseInt(textViewQuantity.getText().toString()), mMaxQuantity, false);
                    if (quantity == 0)return;
                    textViewQuantity.setText(String.valueOf(quantity));
                }
            });
        }

    }

    private int changeQuantity(int quantity, int maxQuantity, boolean add){
        if (add){
            if (quantity >= maxQuantity)return 0;
            quantity++;
        }else {
            if (quantity == 1)return 0;
            quantity--;
        }
        return quantity;
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setIcon(R.drawable.logo)
                .setTitle(R.string.product_added_to_cart)
                .setCancelable(false)
                .setMessage("Allez dans le panier pour modifier la quantité ou finaliser la commande.")
                .setPositiveButton("Panier", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Utilities.openActivity(ProductDetailActivity.this, CartActivity.class.getName());
                        finish();
                    }
                })
                .setNeutralButton("Continuer mes achats", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                        Utilities.showBackwardTransition(ProductDetailActivity.this);
                    }
                });
        if (!isFinishing())builder.create().show();
    }

    private void updateProductQuantity(int id, int quantityInCart, int quantityToAdd) {
        ContentValues values = new ContentValues();
        values.put(CartEntry.COLUMN_QUANTITY, (quantityInCart + quantityToAdd));
        Uri uri = ContentUris.withAppendedId(CONTENT_URI, id);
        getContentResolver().update(uri, values, null, null);
    }

    private void addToCart(int quantity) {
        ContentValues values = new ContentValues();
        values.put(CartEntry.COLUMN_NAME, mProduct.getName());
        values.put(CartEntry.COLUMN_IMAGE, mProduct.getImages().size() > 0 ? mProduct.getImages().get(0).getSrc() : "");
        values.put(CartEntry.COLUMN_PRICE, mProduct.getPrice());
        values.put(CartEntry.COLUMN_MAX_QUANTITY, mProduct.getStock_quantity() == null ? 10 : Integer.parseInt(mProduct.getStock_quantity()));
        values.put(CartEntry.COLUMN_QUANTITY, quantity);
        values.put(CartEntry.COLUMN_PRODUCT_ID, mProduct.getId());

        Log.e(getClass().getName(), "Call to execute - thread: " + Thread.currentThread().getId());
        new CreateNoteTask().execute(values);
    }

    public String stripHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return Html.fromHtml(html,  Html.FROM_HTML_MODE_COMPACT).toString();
        }
        return Html.fromHtml(html).toString();
    }


    public class CreateNoteTask extends AsyncTask<ContentValues, Void, Uri> {

        @Override
        protected Uri doInBackground(ContentValues... contentValues) {
            Log.e(getClass().getName(), "doInBackground - thread: " + Thread.currentThread().getId());
            ContentValues insertValue = contentValues[0];
            return getContentResolver().insert(Cart.CONTENT_URI, insertValue);
        }

        @Override
        protected void onPostExecute(Uri uri) {
            getCartRows();
            Log.e(getClass().getName(), "onPostExecute - thread: " + Thread.currentThread().getId());
        }
    }
}