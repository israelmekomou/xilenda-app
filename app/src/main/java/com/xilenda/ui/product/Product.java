package com.xilenda.ui.product;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [10/07/2020].
 */
public class Product implements Parcelable {
    private int id;
    private int total_sales;
    private int rating_count;
    private String name;
    private String title;
    private String slug;
    private String created_at;
    private String updated_at;
    private String type;
    private String status;
    private String description;
    private String short_description;
    private String price;
    private String regular_price;
    private String sale_price;
    private String average_rating;
    private String stock_quantity;
    private ArrayList<Image> images = new ArrayList<>();

    protected Product(Parcel in) {
        id = in.readInt();
        total_sales = in.readInt();
        rating_count = in.readInt();
        name = in.readString();
        title = in.readString();
        slug = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        type = in.readString();
        status = in.readString();
        description = in.readString();
        short_description = in.readString();
        price = in.readString();
        regular_price = in.readString();
        sale_price = in.readString();
        average_rating = in.readString();
        stock_quantity = in.readString();
        images = in.createTypedArrayList(Image.CREATOR);
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotal_sales() {
        return total_sales;
    }

    public void setTotal_sales(int total_sales) {
        this.total_sales = total_sales;
    }

    public int getRating_count() {
        return rating_count;
    }

    public void setRating_count(int rating_count) {
        this.rating_count = rating_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(String regular_price) {
        this.regular_price = regular_price;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(String average_rating) {
        this.average_rating = average_rating;
    }

    public String getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(String stock_quantity) {
        this.stock_quantity = stock_quantity;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(total_sales);
        parcel.writeInt(rating_count);
        parcel.writeString(name);
        parcel.writeString(title);
        parcel.writeString(slug);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
        parcel.writeString(type);
        parcel.writeString(status);
        parcel.writeString(description);
        parcel.writeString(short_description);
        parcel.writeString(price);
        parcel.writeString(regular_price);
        parcel.writeString(sale_price);
        parcel.writeString(average_rating);
        parcel.writeString(stock_quantity);
        parcel.writeTypedList(images);
    }
}
