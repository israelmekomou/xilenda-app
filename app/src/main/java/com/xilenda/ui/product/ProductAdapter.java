package com.xilenda.ui.product;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xilenda.R;
import com.xilenda.ui.productDetail.ProductDetailActivity;
import com.xilenda.utils.Utils;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Israel MEKOMOU [10/07/2020].
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    public static final String PRODUCT = "Product";
    private ArrayList<Product> mProducts = new ArrayList<>();
    private boolean search = false;
    private Activity mActivity;

    public ProductAdapter() {
    }

    public ProductAdapter(Activity activity, boolean search) {
        this.search = search;
        this.mActivity = activity;
    }

    public void setProducts(ArrayList<Product> products) {
        mProducts = products;
        notifyDataSetChanged();
    }

    public ArrayList<Product> getProducts() {
        return mProducts;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(search ? R.layout.item_product_search : R.layout.item_product_1, parent, false);
        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = mProducts.get(position);
        holder.bind(product);
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImageViewProduct;
        TextView mTextViewProductName;
        TextView mTextViewProductPrice;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageViewProduct = itemView.findViewById(R.id.ivProduct);
            mTextViewProductName = itemView.findViewById(R.id.tvProductName);
            mTextViewProductPrice = itemView.findViewById(R.id.tvProductPrice);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Product product = mProducts.get(getAdapterPosition());
            Intent intent = new Intent(itemView.getContext(), ProductDetailActivity.class);
            intent.putExtra(PRODUCT, product);
            if (mActivity != null){
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity, mImageViewProduct, mActivity.getString(R.string.product_image_transition));
                itemView.getContext().startActivity(intent, optionsCompat.toBundle());
            }
        }

        public void bind(Product product){
            mTextViewProductName.setText(product.getName());
            mTextViewProductPrice.setText(product.getPrice().concat(Utils.CURRENCY));

            if (product.getImages() != null && product.getImages().size() > 0){
                Glide.with(itemView.getContext())
                        .load(product.getImages().get(0).getSrc())
                        .into(mImageViewProduct);
            }
        }
    }
}
