package com.xilenda.ui.product;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Israel MEKOMOU [10/07/2020].
 */
public class Image implements Parcelable {
    private int id;
    private String src;
    private String title;

    protected Image(Parcel in) {
        id = in.readInt();
        src = in.readString();
        title = in.readString();
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(src);
        parcel.writeString(title);
    }
}
