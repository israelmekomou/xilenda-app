package com.xilenda.ui.product;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.ui.home.Category;
import com.xilenda.ui.home.CategoryAdapter;
import com.xilenda.ui.subcategoryProducts.SubCategoryAdapter;
import com.xilenda.uiComponents.SpacingItemDecoration;
import com.xilenda.uiComponents.Tools;
import com.xilenda.uiComponents.Utilities;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Created by Israel MEKOMOU [10/07/2020].
 */
public class ProductActivity extends BaseActivity {

    private ProductViewModel mViewModel;
    private Category mCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Intent intent = getIntent();
        assert intent != null;
        mCategory = intent.getParcelableExtra(SubCategoryAdapter.CATEGORY);
        if (mCategory != null) initUi(mCategory);
    }

    @Override
    protected void onResume() {
        if (mCategory != null)super.initToolbar(this, mCategory.getName(), false, false, false);
        super.onResume();
    }

    private void initUi(final Category category) {
        RecyclerView recyclerViewProducts = findViewById(R.id.rvProducts);
        final ProductAdapter adapter = new ProductAdapter(this, true);
        recyclerViewProducts.setAdapter(adapter);
        recyclerViewProducts.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewProducts.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 8), true));

        mViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        mViewModel.requestProducts(category.getId());
        mViewModel.getProducts().observe(this, new Observer<ArrayList<Product>>() {
            @Override
            public void onChanged(ArrayList<Product> products) {
                if (products != null){
                    if (!products.isEmpty()){
                        adapter.setProducts(products);
                    }else {
                        Toasty.info(ProductActivity.this, getString(R.string.no_item_found)).show();
                    }
                }
            }
        });

        mViewModel.getErrors().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null && !TextUtils.isEmpty(s)){
                    showAlertDialog(s, category.getId());
                }
            }
        });

        mViewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                findViewById(R.id.cvProgress).setVisibility(aBoolean ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void showAlertDialog(String message, final int category){
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_24dp)
                .setTitle(R.string.error_title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mViewModel.requestProducts(category);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        if (!isFinishing())builder.create().show();
    }
}