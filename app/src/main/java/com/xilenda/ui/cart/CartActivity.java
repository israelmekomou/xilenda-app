package com.xilenda.ui.cart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.data.DatabaseContract.CartEntry;
import com.xilenda.data.DatabaseOpenHelper;
import com.xilenda.ui.checkout.CheckoutActivity;
import com.xilenda.ui.orderHistory.ShippingMethod;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.Utils;

import java.util.ArrayList;

import static com.xilenda.data.ProviderContract.Cart.*;

/**
 * Created by Israel MEKOMOU [07/07/2020].
 */
public class CartActivity extends BaseActivity implements
        LoaderManager.LoaderCallbacks<Cursor>
        , OnDatabaseChange{

    public static final int LOADER_CART = 0;
    public static final String SHIPPING_METHOD = "shipping_method";
    public static final String CART_PRICE = "cart_price";
    private DatabaseOpenHelper mDbOpenHelper;
    private CartAdapter mAdapter = new CartAdapter(this);
    private TextView mTextViewCartPrice;
    private TextView mTextViewShippingPrice;
    private CartViewModel mCartViewModel;
    private ArrayList<Cart> mCarts = new ArrayList<>();
    private int mCartPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        mDbOpenHelper = new DatabaseOpenHelper(this);
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        super.initToolbar(this, getString(R.string.title_cart), false, true, true);
        LoaderManager.getInstance(this).restartLoader(LOADER_CART, null, this);
    }

    private void initUi() {
        final RelativeLayout layoutCheckout = findViewById(R.id.lytCheckout);
        final TextView textViewContinue = findViewById(R.id.tvContinue);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        RecyclerView recyclerViewCart = findViewById(R.id.rvCart);
        mTextViewCartPrice = findViewById(R.id.tvCartPrice);
        mTextViewShippingPrice = findViewById(R.id.tvDeliveryPrice);
        mTextViewShippingPrice.setText(String.valueOf(Utils.DELIVERY_PRICE).concat(Utils.CURRENCY));
        recyclerViewCart.setAdapter(mAdapter);
        recyclerViewCart.setLayoutManager(new LinearLayoutManager(this));
        mCartViewModel = new ViewModelProvider(this).get(CartViewModel.class);
        mCartViewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s == null || s.isEmpty())return;
                Utilities.showAlertDialog(CartActivity.this, R.drawable.ic_warning_24dp, getString(R.string.title_warning), s, true);
            }
        });
        mCartViewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                textViewContinue.setVisibility(aBoolean ? View.GONE : View.VISIBLE);
                progressBar.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                layoutCheckout.setBackgroundTintList(ContextCompat.getColorStateList(CartActivity.this, aBoolean ? R.color.grey_40 : R.color.colorPrimary));
            }
        });
        mCartViewModel.getShippingMethod().observe(this, new Observer<ArrayList<ShippingMethod>>() {
            @Override
            public void onChanged(ArrayList<ShippingMethod> shippingMethods) {
                if (shippingMethods != null) {
                    Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                    for (ShippingMethod s : shippingMethods){
                        if (s.getId().equals("flat_rate")) {
                            intent.putExtra(SHIPPING_METHOD, s);
                            break;
                        }
                    }
                    intent.putExtra(CART_PRICE, mCartPrice);
                    startActivity(intent);
                    Utilities.showForwardTransition(CartActivity.this);
                }
            }
        });
        layoutCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCarts != null && mCarts.size() > 0){
                    mCartViewModel.requestShippingMethods();
                }else {
                    Snackbar.make(view, "Veuillez ajouter aumoins un élément au panier.", 7000).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (mDbOpenHelper != null)mDbOpenHelper.close();
        super.onDestroy();
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        CursorLoader loader = null;
        if (id == LOADER_CART){
            String[] cartColumns = {
                    _ID,
                    COLUMN_NAME,
                    COLUMN_IMAGE,
                    COLUMN_PRICE,
                    COLUMN_QUANTITY,
                    COLUMN_PRODUCT_ID,
                    COLUMN_MAX_QUANTITY,
                    COLUMN_ATTRIBUTES
            };

            loader = new CursorLoader(this, CONTENT_URI, cartColumns, null, null, null);
        }
        assert loader != null;
        return loader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == LOADER_CART){
            assert data != null;
            mCarts = new ArrayList<>();
            data.moveToFirst();
            while (!data.isAfterLast()){
                Cart cart = new Cart();
                cart.setId(data.getInt(data.getColumnIndex(CartEntry._ID)));
                cart.setQuantity(data.getInt(data.getColumnIndex(CartEntry.COLUMN_QUANTITY)));
                cart.setMax_quantity(data.getInt(data.getColumnIndex(CartEntry.COLUMN_MAX_QUANTITY)));
                cart.setProduct_id(data.getInt(data.getColumnIndex(CartEntry.COLUMN_PRODUCT_ID)));
                cart.setPrice(data.getInt(data.getColumnIndex(CartEntry.COLUMN_PRICE)));
                cart.setName(data.getString(data.getColumnIndex(CartEntry.COLUMN_NAME)));
                cart.setImage(data.getString(data.getColumnIndex(CartEntry.COLUMN_IMAGE)));
                mCarts.add(cart);
                data.moveToNext();
            }
            calculateCartPrice(mCarts);
            mAdapter.setCarts(mCarts);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        if (loader.getId() == LOADER_CART)mAdapter.setCarts(null);
    }

    private void calculateCartPrice(ArrayList<Cart> carts){
        mCartPrice = 0;
        for (Cart c : carts){
            mCartPrice = mCartPrice + (c.getPrice() * c.getQuantity());
        }
        mTextViewCartPrice.setText(String.valueOf(mCartPrice).concat(Utils.CURRENCY));
    }

    @Override
    public void calculatePrice() {
        Log.e("CALCULATE", "TRUE");
        LoaderManager.getInstance(this).restartLoader(LOADER_CART, null, this);
    }
}