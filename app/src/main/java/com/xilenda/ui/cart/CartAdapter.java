package com.xilenda.ui.cart;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.xilenda.R;
import com.xilenda.data.DatabaseContract;
import com.xilenda.data.DatabaseContract.CartEntry;
import com.xilenda.data.ProviderContract;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.Utils;

import java.util.ArrayList;

import static com.xilenda.data.ProviderContract.Cart.*;

/**
 * Created by Israel MEKOMOU [11/07/2020].
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {
    ArrayList<Cart> mCarts = new ArrayList<>();
    Context mContext;
    private final OnDatabaseChange mDatabaseChange;

    public CartAdapter(Context context) {
        mContext = context;
        mDatabaseChange = (OnDatabaseChange) context;
    }

    public void setCarts(ArrayList<Cart> carts) {
        mCarts = carts;
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_cart, parent, false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mCarts.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        ImageView mImageViewCart;
        TextView mTextViewName;
        TextView mTextViewPrice;
        TextView mTextViewDelete;
        TextView mTextViewQuantity;
        ImageButton mImageButtonAdd;
        ImageButton mImageButtonRemove;
        Uri mCartUri;
        int maxQuantity = 0;
        int mPosition;

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageViewCart = itemView.findViewById(R.id.ivCart);
            mTextViewName = itemView.findViewById(R.id.tvName);
            mTextViewPrice = itemView.findViewById(R.id.tvPrice);
            mTextViewDelete = itemView.findViewById(R.id.tvDelete);
            mTextViewQuantity = itemView.findViewById(R.id.tvQuantity);
            mImageButtonAdd = itemView.findViewById(R.id.ibAdd);
            mImageButtonRemove = itemView.findViewById(R.id.ibRemove);

            mTextViewDelete.setOnClickListener(this);
            mImageButtonAdd.setOnClickListener(this);
            mImageButtonRemove.setOnClickListener(this);
        }

        public void bind(int i){
            mPosition = i;
            Cart cart = mCarts.get(i);
            assert cart != null;
            mTextViewName.setText(cart.getName());
            mTextViewPrice.setText(String.valueOf(cart.getPrice()).concat(Utils.CURRENCY));
            mTextViewQuantity.setText(String.valueOf(cart.getQuantity()));
            maxQuantity = cart.getMax_quantity();
            mCartUri = ContentUris.withAppendedId(CONTENT_URI, cart.getId());
            Utilities.displayImageFromUrl(itemView.getContext(), cart.getImage(), mImageViewCart);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.tvDelete :
                    deleteItem();
                    break;
                case R.id.ibAdd :
                    changeQuantity(Integer.parseInt(mTextViewQuantity.getText().toString()), true);
                    break;
                case R.id.ibRemove :
                    changeQuantity(Integer.parseInt(mTextViewQuantity.getText().toString()), false);
                    break;
            }
        }

        private void deleteItem() {
            AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext())
                    .setIcon(R.drawable.ic_warning_24dp)
                    .setTitle(R.string.title_warning)
                    .setMessage("Voulez vous supprimer ce produit de votre panier?")
                    .setPositiveButton("Supprimer", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            itemView.getContext().getContentResolver().delete(mCartUri, null, null);
                            notifyItemRemoved(mPosition);
                            mCarts.remove(mPosition);
                            mDatabaseChange.calculatePrice();
                            Snackbar.make(itemView, "Ce produit à été supprimé", 3000).show();
                        }
                    })
                    .setNeutralButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            builder.create().show();

        }

        private void changeQuantity(int quantity, boolean add){
            if (add){
                if (quantity == maxQuantity)return;
                quantity = quantity + 1;
            }else {
                if (quantity == 1)return;
                quantity = quantity -1;

            }
            Log.e("TEST", "TEST1");
            updateRowInDatabase(quantity);
            Log.e("TEST", "TEST2");
            mDatabaseChange.calculatePrice();
            Log.e("TEST", "TEST3");
        }

        private void updateRowInDatabase(int quantity) {
            Log.e("TEST", "TEST4");
            ContentValues values = new ContentValues();
            Log.e("TEST", "TEST5");
            values.put(CartEntry.COLUMN_QUANTITY, quantity);
            Log.e("TEST", "TEST6");
            itemView.getContext().getContentResolver().update(mCartUri, values, null, null);
            Log.e("TEST", "TEST7");
            mTextViewQuantity.setText(String.valueOf(quantity));
            Log.e("TEST", "TEST8");
        }
    }
}
