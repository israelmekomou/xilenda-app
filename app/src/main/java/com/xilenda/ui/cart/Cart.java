package com.xilenda.ui.cart;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Israel MEKOMOU [11/07/2020].
 */
public class Cart implements Parcelable {
    private int id;
    private String name;
    private String image;
    private int price;
    private int quantity;
    private int max_quantity;
    private int product_id;

    protected Cart(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
        price = in.readInt();
        quantity = in.readInt();
        max_quantity = in.readInt();
        product_id = in.readInt();
    }

    public static final Creator<Cart> CREATOR = new Creator<Cart>() {
        @Override
        public Cart createFromParcel(Parcel in) {
            return new Cart(in);
        }

        @Override
        public Cart[] newArray(int size) {
            return new Cart[size];
        }
    };

    public Cart() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMax_quantity() {
        return max_quantity;
    }

    public void setMax_quantity(int max_quantity) {
        this.max_quantity = max_quantity;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(image);
        parcel.writeInt(price);
        parcel.writeInt(quantity);
        parcel.writeInt(max_quantity);
        parcel.writeInt(product_id);
    }
}
