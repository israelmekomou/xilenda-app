package com.xilenda.ui.cart;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xilenda.ui.orderHistory.ShippingMethod;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [22/07/2020].
 */
public class CartViewModel extends ViewModel {
    MutableLiveData<String> mError;
    MutableLiveData<Boolean> mLoad;
    MutableLiveData<ArrayList<ShippingMethod>> mShippingMethods;

    public CartViewModel() {
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
        mShippingMethods = new MutableLiveData<>();
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public MutableLiveData<ArrayList<ShippingMethod>> getShippingMethod() {
        return mShippingMethods;
    }

    public void requestShippingMethods(){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getOrderApi().getShippingMethods();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String output = response.body().string();
                        ArrayList<ShippingMethod> shippingMethods = new Gson().fromJson(output, new TypeToken<ArrayList<ShippingMethod>>(){}.getType());
                        mShippingMethods.setValue(shippingMethods);
                    } catch (IOException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    mError.setValue(Utils.API_ERROR_MESSAGE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
                mLoad.setValue(false);
            }
        });
    }
}
