package com.xilenda.ui.cart;

/**
 * Created by Israel MEKOMOU [13/07/2020].
 */
public interface OnDatabaseChange {
    void calculatePrice();
}
