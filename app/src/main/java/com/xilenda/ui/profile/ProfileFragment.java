package com.xilenda.ui.profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.utils.PreferenceUtils;

/**
 * Created by Israel MEKOMOU [23/07/2020].
 */
public class ProfileFragment extends Fragment{

    public static final String EDIT_USER = "edit_user";
    private ProfileViewModel slideshowViewModel;
    private View mRoot;
    private TextView mTextViewName;
    private TextView mTextViewPhone;
    private TextView mTextViewEmail;
    private TextView mTextViewAddress;
    private TextView mTextViewAddressDesc;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        mRoot = inflater.inflate(R.layout.fragment_profile, container, false);
        return mRoot;
    }

    @Override
    public void onResume() {
        super.onResume();
        initUi(mRoot);
    }

    private void initUi(View root) {
        mTextViewName = root.findViewById(R.id.tvName);
        mTextViewPhone = root.findViewById(R.id.tvPhone);
        mTextViewEmail = root.findViewById(R.id.tvEmail);
        mTextViewAddress = root.findViewById(R.id.tvAddress);
        mTextViewAddressDesc = root.findViewById(R.id.tvAddressDesc);
        ProfileViewModel viewModel = new ViewModelProvider(getActivity()).get(ProfileViewModel.class);
        viewModel.getIsDismissed().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (!aBoolean)return;
                Customer customer = CustomerHelper.getCurrentCustomer(mRoot.getContext());
                setCustomer(customer);
            }
        });
        Button button = root.findViewById(R.id.buttonEditProfile);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditProfileDialog dialog = new EditProfileDialog();
                Fragment fragment = getChildFragmentManager().findFragmentByTag(EDIT_USER);
                if (fragment == null)dialog.show(getChildFragmentManager(), EDIT_USER);

            }
        });

        Customer customer = CustomerHelper.getCurrentCustomer(mRoot.getContext());
        setCustomer(customer);
    }

    private void setCustomer(Customer customer) {
        if (customer == null)return;
        mTextViewName.setText(customer.getFirst_name());
        mTextViewEmail.setText(customer.getEmail());
        if (customer.getBilling() == null)return;
        mTextViewPhone.setText(customer.getBilling().getPhone());
        mTextViewAddress.setText(customer.getBilling().getAddress_1());
        mTextViewAddressDesc.setText(customer.getBilling().getState());
    }

}