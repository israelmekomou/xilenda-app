package com.xilenda.ui.profile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.ui.orderHistory.Address;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.PreferenceUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [23/07/2020].
 */
public class EditProfileDialog extends AppCompatDialogFragment {
    TextInputEditText mEditTextName;
    TextInputEditText mEditTextPhone;
    TextInputEditText mEditTextAddress;
    TextInputEditText mEditTextAddressDesc;
    ProgressBar mProgressBar;
    Button mButton;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_edit_profile, null);
        builder.setView(v);
        initUi(v);
        return builder.create();
    }

    private void initUi(View view) {
        mEditTextName = view.findViewById(R.id.tietUserFullName);
        mEditTextPhone = view.findViewById(R.id.tietUserPhone);
        mEditTextAddress = view.findViewById(R.id.tietUserAddress);
        mEditTextAddressDesc = view.findViewById(R.id.tietUserAddressDesc);
        mProgressBar = view.findViewById(R.id.progressBar);
        mButton = view.findViewById(R.id.btnSave);

        final Customer customer = CustomerHelper.getCurrentCustomer(view.getContext());
        if (customer == null) {
            dismiss();
            return;
        }
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!empty()) {
                    saveUserProfile(customer.getId());
                } else {
                    Toasty.info(view.getContext(), getString(R.string.set_at_least_one_field)).show();
                }
            }
        });
        mEditTextName.setText(customer.getFirst_name());
        if (customer.getBilling() == null) return;
        mEditTextPhone.setText(customer.getBilling().getPhone());
        mEditTextAddress.setText(customer.getBilling().getAddress_1());
        mEditTextAddressDesc.setText(customer.getBilling().getState());
    }

    private void saveUserProfile(int id) {
        Utilities.hideKeyBoard(requireActivity());
        Customer customer = new Customer();
        customer.setId(id);
        customer.setFirst_name(mEditTextName.getText().toString());
        Address billing = new Address();
        billing.setAddress_1(mEditTextAddress.getText().toString());
        billing.setState(mEditTextAddressDesc.getText().toString());
        billing.setPhone(mEditTextPhone.getText().toString());
        customer.setBilling(billing);
        Call<ResponseBody> call = ApiUtils.getAuthenticationService().editUserInfo(id, Utils.CONSUMER_KEY, Utils.CONSUMER_SECRETE, customer);
        mProgressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mProgressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        String output = response.body().string();
                        Customer newCustomer = new Gson().fromJson(new JsonParser().parse(output), Customer.class);
                        CustomerHelper.setCurrentCustomer(mProgressBar.getContext(), newCustomer);
                        Toasty.success(mProgressBar.getContext(), getString(R.string.update_saved)).show();
                        ProfileViewModel viewModel = new ViewModelProvider(getActivity()).get(ProfileViewModel.class);
                        viewModel.getIsDismissed().setValue(true);
                        dismiss();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toasty.info(mProgressBar.getContext(), Utils.API_ERROR_MESSAGE).show();
                    }
                } else {
                    if (response.errorBody() != null) {
                        try {
                            String output = response.errorBody().string();
                            JSONObject object = new JSONObject(output);
                            Toasty.info(mProgressBar.getContext(), object.getString("message")).show();
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            Toasty.info(mProgressBar.getContext(), Utils.API_ERROR_MESSAGE).show();
                        }
                    } else {
                        Toasty.info(mProgressBar.getContext(), Utils.API_ERROR_MESSAGE).show();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                Toasty.info(mProgressBar.getContext(), getString(R.string.no_network_connection)).show();
            }
        });
    }

    private boolean empty() {
        return mEditTextName.getText().toString().isEmpty() &&
                mEditTextPhone.getText().toString().isEmpty() &&
                mEditTextAddress.getText().toString().isEmpty() &&
                mEditTextAddressDesc.getText().toString().isEmpty();
    }
}