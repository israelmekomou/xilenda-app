package com.xilenda.ui.profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ProfileViewModel extends ViewModel {
    private MutableLiveData<Boolean> isDismissed;

    public ProfileViewModel() {
        isDismissed = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> getIsDismissed() {
        return isDismissed;
    }
}