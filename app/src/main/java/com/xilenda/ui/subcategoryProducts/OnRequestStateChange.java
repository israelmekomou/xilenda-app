package com.xilenda.ui.subcategoryProducts;

/**
 * Created by Israel MEKOMOU [20/07/2020].
 */
public interface OnRequestStateChange {
    void requestFinished(boolean networkIssue, boolean hasItems);
    void requestStarted();
}
