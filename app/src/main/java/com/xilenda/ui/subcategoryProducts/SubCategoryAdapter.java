package com.xilenda.ui.subcategoryProducts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xilenda.R;
import com.xilenda.ui.home.Category;
import com.xilenda.ui.product.Product;
import com.xilenda.ui.product.ProductActivity;
import com.xilenda.ui.product.ProductAdapter;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [12/07/2020].
 */
public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubcategoryViewHolder> {
    public static final String CATEGORY = "category";
    private ArrayList<Category> mCategories = new ArrayList<>();
    private Activity mActivity;

    public SubCategoryAdapter(Activity activity) {
        mActivity = activity;
    }

    public void setCategories(ArrayList<Category> categories) {
        mCategories = categories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SubcategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_rv_subcategory, parent, false);
        return new SubcategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubcategoryViewHolder holder, int position) {
        Category category = mCategories.get(position);
        holder.bind(category);
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    public class SubcategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTextViewSubcategoryName;
        private TextView mTextViewViewAll;
        private RecyclerView mRecyclerViewProducts;
        private ProductAdapter mAdapter;
        private OnRequestStateChange mOnRequestFinished;
        private Category mCategory;

        public SubcategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextViewSubcategoryName = itemView.findViewById(R.id.tvSubcategoryName);
            mTextViewViewAll = itemView.findViewById(R.id.tvViewAll);
            mRecyclerViewProducts = itemView.findViewById(R.id.rvProducts);
            mOnRequestFinished = ((OnRequestStateChange) itemView.getContext());
            mTextViewViewAll.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Category category = mCategories.get(getAdapterPosition());
            Intent intent = new Intent(itemView.getContext(), ProductActivity.class);
            intent.putExtra(CATEGORY, category);
            itemView.getContext().startActivity(intent);
            Utilities.showForwardTransition(mActivity);
        }

        public void bind(Category category){
            mCategory = category;
            itemView.setVisibility(View.GONE);
            int position = getAdapterPosition();
            mTextViewSubcategoryName.setText(category.getName());
            mRecyclerViewProducts.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
            mAdapter = new ProductAdapter(mActivity, false);
            mRecyclerViewProducts.setAdapter(mAdapter);
            if (category.getProducts() == null) {
                requestProducts();
            } else {
                mAdapter.setProducts(category.getProducts());
                itemView.setVisibility(View.VISIBLE);
                mTextViewViewAll.setVisibility(category.getProducts().size() > 9 ? View.VISIBLE : View.GONE);
            }
        }

        private void requestProducts() {
            mOnRequestFinished.requestStarted();
            Call<ResponseBody> call = ApiUtils.getCProductsService().getProductsFromCategory(
                    mCategory.getId(),
                    10
            );
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                    String responseString = "";
                    boolean hasItems = false;
                    if (response.isSuccessful() && response.body() != null){
                        try {
                            responseString = response.body().string();
                            JSONArray object = new JSONArray(responseString);
                            ArrayList<Product> products = new Gson().fromJson(object.toString(), new TypeToken<ArrayList<Product>>(){}.getType());
                            if (products != null && !products.isEmpty()){
                                mAdapter.setProducts(products);
                                itemView.setVisibility(View.VISIBLE);
                                for (int i = 0; i < mCategories.size(); i++) {
                                    Category c = mCategories.get(i);
                                    if (c.getId() == mCategory.getId()){
                                        mCategories.get(i).setProducts(products);
                                    }
                                }
                                hasItems = true;
                                mTextViewViewAll.setVisibility(products.size() > 9 ? View.VISIBLE : View.GONE);
                            }else {
                                try{
                                    mCategories.remove(getAdapterPosition());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                notifyItemRemoved(getAdapterPosition());
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    mOnRequestFinished.requestFinished(false, hasItems);
                }

                @Override
                public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                    t.printStackTrace();
                    mOnRequestFinished.requestFinished(true, false);
//                    mError.setValue(Utils.ON_FAILURE_MESSAGE);
                }
            });
        }
    }
}
