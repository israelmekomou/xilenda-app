package com.xilenda.ui.subcategoryProducts;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.ui.home.Category;
import com.xilenda.ui.home.CategoryAdapter;
import com.xilenda.uiComponents.Utilities;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Created by Israel MEKOMOU [12/07/2020].
 */
public class SubcategoryActivity extends BaseActivity implements OnRequestStateChange {

    private Intent mIntent;
    private int mRequestStarted = 0;
    private int mRequestFinished = 0;
    private CardView mCardViewProgress;
    private boolean mIsEmpty = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        initUi();
    }

    private void initUi() {
        RecyclerView recyclerViewSubCategories = findViewById(R.id.rvSubcategories);
        mCardViewProgress = findViewById(R.id.cvProgress);
        recyclerViewSubCategories.setLayoutManager(new LinearLayoutManager(this));
        SubCategoryAdapter adapter = new SubCategoryAdapter(this);
        recyclerViewSubCategories.setAdapter(adapter);
        mIntent = getIntent();
        ArrayList<Category> categories = mIntent.getParcelableArrayListExtra(CategoryAdapter.SUBCATEGORIES);
        if (categories == null)return;
        Log.e("CATEGORIES", categories.toString());
        if (!categories.isEmpty()){
            adapter.setCategories(categories);
        }else {
            mCardViewProgress.setVisibility(View.GONE);
            Utilities.showAlertDialog(SubcategoryActivity.this, R.drawable.logo, getString(R.string.no_search_item_found), getString(R.string.no_item_found), true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIntent = getIntent();
        assert mIntent != null;
        super.initToolbar(this, mIntent.getStringExtra(SubCategoryAdapter.CATEGORY), false, false, false);
    }

    @Override
    public void requestFinished(boolean networkIssue, boolean hasItems) {
        mRequestFinished++;
        if (mRequestStarted == mRequestFinished){
            mCardViewProgress.setVisibility(View.GONE);
            if (mIsEmpty && !networkIssue)Utilities.showAlertDialog(SubcategoryActivity.this, R.drawable.logo, getString(R.string.no_search_item_found), getString(R.string.no_item_found), true);
        }
        if (hasItems && !networkIssue){
            mIsEmpty = false;
            Log.e("TEST", "HAS_ITEMS");
        }
        if (mRequestStarted == mRequestFinished && networkIssue){
            Toasty.info(this, getString(R.string.network_issue)).show();
        }
        Log.e("REQUEST_FINISH", String.valueOf(mRequestFinished));
    }

    @Override
    public void requestStarted() {
        mRequestStarted++;
        Log.e("REQUEST_START", String.valueOf(mRequestStarted));
    }
}