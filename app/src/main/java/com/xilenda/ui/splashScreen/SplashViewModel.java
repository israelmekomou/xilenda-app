package com.xilenda.ui.splashScreen;

import android.app.Activity;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.PreferenceUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.util.Patterns.EMAIL_ADDRESS;

/**
 * Created by Israel MEKOMOU [17/07/2020].
 */
public class SplashViewModel extends ViewModel {
    private MutableLiveData<Boolean> mIsAuthenticated;

    public SplashViewModel() {
        mIsAuthenticated = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> isAuthenticated() {
        return mIsAuthenticated;
    }

    public void getUserInfo(final Activity activity, final int id){
        Call<ResponseBody> call = ApiUtils.getAuthenticationService().getUserInfo(id, Utils.CONSUMER_KEY, Utils.CONSUMER_SECRETE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String output = response.body().string();
                        Customer customer = new Gson().fromJson(new JsonParser().parse(output), Customer.class);
                        CustomerHelper.setCurrentCustomer(activity, customer);
                        PreferenceUtils.setInt(activity, Utils.CUSTOMER_ID, id);
                        Utilities.openActivity(activity, MainActivity.class.getName());
                        Toasty.success(activity, activity.getString(R.string.welcome_message)).show();
                        mIsAuthenticated.setValue(true);
                    } catch (IOException e) {
                        e.printStackTrace();
                        mIsAuthenticated.setValue(false);
                    }
                }else {
                    mIsAuthenticated.setValue(false);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mIsAuthenticated.setValue(false);
            }
        });
    }
}
