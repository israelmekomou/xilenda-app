package com.xilenda.ui.splashScreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Toast;

import com.xilenda.BaseActivity;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.data.DatabaseContract;
import com.xilenda.data.DatabaseOpenHelper;
import com.xilenda.data.ProviderContract;
import com.xilenda.ui.authentication.AuthActivity;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.PreferenceUtils;
import com.xilenda.utils.Utils;

import es.dmoral.toasty.Toasty;

/**
 * Created by Israel MEKOMOU [17/07/2020].
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initUi();
    }

    private void initUi() {
        SplashViewModel viewModel = new ViewModelProvider(this).get(SplashViewModel.class);
        viewModel.isAuthenticated().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean){
                    Toasty.success(SplashActivity.this, getString(R.string.welcome_message), Toast.LENGTH_SHORT).show();
                    Utilities.openActivity(SplashActivity.this, MainActivity.class.getName());
                }else {
                    PreferenceUtils.clear(SplashActivity.this);
                    new DatabaseOpenHelper(SplashActivity.this).deleteTable(ProviderContract.Cart.TABLE_NAME);
                    CustomerHelper.setCurrentCustomer(SplashActivity.this, null);
                    Utilities.openActivity(SplashActivity.this, AuthActivity.class.getName());
                }
            }
        });
        viewModel.getUserInfo(this,
                PreferenceUtils.getInt(this, Utils.CUSTOMER_ID));
    }
}