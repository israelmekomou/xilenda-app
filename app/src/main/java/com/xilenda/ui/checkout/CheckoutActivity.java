package com.xilenda.ui.checkout;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.ui.cart.Cart;
import com.xilenda.ui.cart.CartActivity;
import com.xilenda.ui.orderHistory.Address;
import com.xilenda.ui.orderHistory.LineItem;
import com.xilenda.ui.orderHistory.Order;
import com.xilenda.ui.orderHistory.PaymentDetails;
import com.xilenda.ui.orderHistory.ShippingMethod;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [08/07/2020].
 */
public class CheckoutActivity extends BaseActivity {
    private TextInputEditText mEditTextName;
    private TextInputEditText mEditTextPhone;
    private TextInputEditText mEditTextEmail;
    private TextInputEditText mEditTextAddress;
    private TextInputEditText mEditTextAddressDesc;
    private TextInputEditText mEditTextOrderNote;
    private boolean isLoading = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        initUi();
        initOrderButton();
    }

    private void initOrderButton() {
        final CheckoutViewModel viewModel = new ViewModelProvider(this).get(CheckoutViewModel.class);
        final CardView cardViewOrder = findViewById(R.id.cvOrder);
        final TextView textViewOrder = findViewById(R.id.tvOrder);
        final ProgressBar progressBar = findViewById(R.id.progressBar);

        viewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                isLoading = aBoolean;
                progressBar.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                textViewOrder.setVisibility(aBoolean ? View.GONE : View.VISIBLE);
                cardViewOrder.setCardBackgroundColor(
                        aBoolean ? ContextCompat.getColor(CheckoutActivity.this, R.color.grey_40)
                        : ContextCompat.getColor(CheckoutActivity.this, R.color.colorPrimary)
                );
                mEditTextName.setEnabled(!aBoolean);
                mEditTextPhone.setEnabled(!aBoolean);
                mEditTextEmail.setEnabled(!aBoolean);
                mEditTextAddress.setEnabled(!aBoolean);
                mEditTextAddressDesc.setEnabled(!aBoolean);
                mEditTextOrderNote.setEnabled(!aBoolean);
                cardViewOrder.setEnabled(!aBoolean);
            }
        });

        viewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                assert s != null;
                Utilities.showAlertDialog(CheckoutActivity.this, R.drawable.ic_warning_24dp, getString(R.string.error_title), s, true);
            }
        });

        cardViewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (verifyData()){
                    Log.e("TEST", "OK");
                    initOrder(viewModel);
                }
            }
        });
    }

    private void initOrder(CheckoutViewModel viewModel){
        ArrayList<LineItem> lineItems = new ArrayList<>();
        int subtotal = 0;
        for (Cart c : getCartItems()){
            LineItem item = new LineItem();
            item.setProduct_id(c.getProduct_id());
            item.setQuantity(c.getQuantity());
            subtotal += c.getPrice();
            lineItems.add(item);
        }

        Address billingAddress = new Address();
        billingAddress.setFirst_name(mEditTextName.getText().toString());
        billingAddress.setPhone(mEditTextPhone.getText().toString());
        billingAddress.setEmail(mEditTextEmail.getText().toString());
        billingAddress.setAddress_1(mEditTextAddress.getText().toString());
        billingAddress.setState(mEditTextAddressDesc.getText().toString());

        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setMethod_id("");
        paymentDetails.setMethod_title("");
        paymentDetails.setPaid(false);
        ShippingMethod shippingMethod = new ShippingMethod();
        if (getIntent() != null && getIntent().hasExtra(CartActivity.SHIPPING_METHOD)){
            shippingMethod = getIntent().getParcelableExtra(CartActivity.SHIPPING_METHOD);
        }
        Customer customer = CustomerHelper.getCurrentCustomer(this);
        Order order = new Order();
        if (customer != null) order.setCustomer_id(customer.getId());
        order.setNumber(getString(R.string.order_number));
        order.setStatus(getString(R.string.on_hold));
        order.setBilling(billingAddress);
        order.setShipping(billingAddress);
        order.setPayment_details(paymentDetails);
        order.setShipping_methods(shippingMethod);
        order.setLine_items(lineItems);
        int total = subtotal + Utils.DELIVERY_PRICE;
        order.setTotal(String.valueOf(total));
        order.setShipping_total(String.valueOf(Utils.DELIVERY_PRICE));
        order.setCustomer_note(mEditTextOrderNote.getText().toString());
        viewModel.placeOrder(order, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        super.initToolbar(this, getString(R.string.app_name), false, true, true);
        Customer customer = CustomerHelper.getCurrentCustomer(this);
        if (customer == null) return;
        mEditTextName.setText(customer.getFirst_name());
        mEditTextEmail.setText(customer.getEmail());
        if (customer.getBilling() == null)return;
        mEditTextPhone.setText(customer.getBilling().getPhone());
        mEditTextAddress.setText(customer.getBilling().getAddress_1());
        mEditTextAddressDesc.setText(customer.getBilling().getState());
    }

    private void initUi() {
        Utilities.hideKeyBoard(this);
        mEditTextName = findViewById(R.id.tietShippingName);
        mEditTextPhone = findViewById(R.id.tietShippingPhone);
        mEditTextEmail = findViewById(R.id.tietShippingEmail);
        mEditTextAddress = findViewById(R.id.tietShippingAddress);
        mEditTextAddressDesc = findViewById(R.id.tietShippingAddressDesc);
        mEditTextOrderNote = findViewById(R.id.tietOrderNote);
        TextView textViewSubtotal = findViewById(R.id.tvSubtotal);
        TextView textViewShippingPrice = findViewById(R.id.tvShippingPrice);
        TextView textViewTotal = findViewById(R.id.tvTotal);

        if (getIntent() != null && getIntent().hasExtra(CartActivity.CART_PRICE)){
            int cartPrice = getIntent().getIntExtra(CartActivity.CART_PRICE, 0);
            textViewSubtotal.setText(String.valueOf(cartPrice).concat(Utils.CURRENCY));
            textViewShippingPrice.setText(String.valueOf(Utils.DELIVERY_PRICE).concat(Utils.CURRENCY));

            int total = cartPrice + Utils.DELIVERY_PRICE;
            textViewTotal.setText(String.valueOf(total).concat(Utils.CURRENCY));
        }

    }

    private boolean verifyData(){
        boolean isOk = true;
        if (TextUtils.isEmpty(mEditTextName.getText())){
            mEditTextName.setError(getString(R.string.field_is_mandatory));
            isOk = false;
        }else {
            mEditTextName.setError(null);
        }
        if (TextUtils.isEmpty(mEditTextPhone.getText())){
            mEditTextPhone.setError(getString(R.string.field_is_mandatory));
            isOk = false;
        }else {
            mEditTextPhone.setError(null);
        }
        if (TextUtils.isEmpty(mEditTextAddress.getText())){
            mEditTextAddress.setError(getString(R.string.field_is_mandatory));
            isOk = false;
        }else {
            mEditTextAddress.setError(null);
        }
        if (TextUtils.isEmpty(mEditTextAddressDesc.getText())){
            mEditTextAddressDesc.setError(getString(R.string.field_is_mandatory));
            isOk = false;
        }else {
            mEditTextAddressDesc.setError(null);
        }

        Log.e("IS", String.valueOf(isOk));

        return isOk;
    }

    @Override
    public void onBackPressed() {
        if (isLoading)return;
        super.onBackPressed();
    }
}