package com.xilenda.ui.checkout;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.data.DatabaseContract.CartEntry;
import com.xilenda.data.DatabaseOpenHelper;
import com.xilenda.ui.orderHistory.Order;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [14/07/2020].
 */
public class CheckoutViewModel extends ViewModel {
    private MutableLiveData<String> mError;
    private MutableLiveData<Boolean> mLoad;

    public CheckoutViewModel() {
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public void placeOrder(Order order, final Activity context){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getOrderApi().create(order);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    new DatabaseOpenHelper(context).deleteTable(CartEntry.TABLE_NAME);
                    Utilities.openActivity(context, MainActivity.class.getName());
                    Toasty.success(context, context.getString(R.string.order_saved), Toast.LENGTH_SHORT).show();
                    context.finish();
                }else {
                    mError.setValue(Utils.API_ERROR_MESSAGE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mLoad.setValue(false);
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
            }
        });
    }
}
