package com.xilenda.ui.orderHistory;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Israel MEKOMOU [14/07/2020].
 */
public class LineItem implements Parcelable {
    private String subtotal;
    private String subtotal_tax;
    private String total;
    private String totalTax;
    private String price;
    private int quantity;
    private String taxClass;
    private String name;
    private int product_id;

    public LineItem() {
    }

    protected LineItem(Parcel in) {
        subtotal = in.readString();
        subtotal_tax = in.readString();
        total = in.readString();
        totalTax = in.readString();
        price = in.readString();
        quantity = in.readInt();
        taxClass = in.readString();
        name = in.readString();
        product_id = in.readInt();
    }

    public static final Creator<LineItem> CREATOR = new Creator<LineItem>() {
        @Override
        public LineItem createFromParcel(Parcel in) {
            return new LineItem(in);
        }

        @Override
        public LineItem[] newArray(int size) {
            return new LineItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(subtotal);
        parcel.writeString(subtotal_tax);
        parcel.writeString(total);
        parcel.writeString(totalTax);
        parcel.writeString(price);
        parcel.writeInt(quantity);
        parcel.writeString(taxClass);
        parcel.writeString(name);
        parcel.writeInt(product_id);
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotal_tax() {
        return subtotal_tax;
    }

    public void setSubtotal_tax(String subtotal_tax) {
        this.subtotal_tax = subtotal_tax;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    @Override
    public String toString() {
        return "LineItem{" +
                "subtotal='" + subtotal + '\'' +
                ", subtotal_tax='" + subtotal_tax + '\'' +
                ", total='" + total + '\'' +
                ", totalTax='" + totalTax + '\'' +
                ", price='" + price + '\'' +
                ", quantity=" + quantity +
                ", taxClass='" + taxClass + '\'' +
                ", name='" + name + '\'' +
                ", product_id=" + product_id +
                '}';
    }
}
