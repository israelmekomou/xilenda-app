package com.xilenda.ui.orderHistory;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class OrderHistoryViewModel extends ViewModel {

    private MutableLiveData<ArrayList<Order>> mOrders;
    private MutableLiveData<String> mError;
    private MutableLiveData<Boolean> mLoad;

    public OrderHistoryViewModel() {
        mOrders = new MutableLiveData<>();
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
    }

    public MutableLiveData<ArrayList<Order>> getOrders() {
        return mOrders;
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public void requestOrders(int customerId){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getOrderApi().getOrders(customerId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String output = response.body().string();
                        JSONArray ordersJSON = new JSONArray(output);
                        ArrayList<Order> orders = new Gson().fromJson(ordersJSON.toString(), new TypeToken<ArrayList<Order>>(){}.getType());
                        mOrders.setValue(orders);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    mError.setValue(Utils.API_ERROR_MESSAGE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mLoad.setValue(false);
                mError.setValue(Utils.API_ERROR_MESSAGE);
            }
        });
    }
}