package com.xilenda.ui.orderHistory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class OrderHistoryFragment extends Fragment {

    private OrderHistoryViewModel mHistoryViewModel;
    private Customer mCustomer;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_order_history, container, false);
        initUi(root);
        return root;
    }

    private void initUi(View root) {
        final OrderAdapter adapter = new OrderAdapter(getChildFragmentManager());
        RecyclerView recyclerViewOrder = root.findViewById(R.id.rvOrders);
        final CardView cardViewProgress = root.findViewById(R.id.cvProgress);
        recyclerViewOrder.setAdapter(adapter);
        recyclerViewOrder.setLayoutManager(new LinearLayoutManager(root.getContext()));

        mHistoryViewModel = new ViewModelProvider(this).get(OrderHistoryViewModel.class);
        mHistoryViewModel.getOrders().observe(getViewLifecycleOwner(), new Observer<ArrayList<Order>>() {
            @Override
            public void onChanged(ArrayList<Order> orders) {
                if(orders != null)adapter.setOrders(orders);
            }
        });
        mHistoryViewModel.getLoad().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                cardViewProgress.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
            }
        });
        mHistoryViewModel.getError().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null){
                    showAlertDialog(s);
                }
            }
        });
        mCustomer = CustomerHelper.getCurrentCustomer(root.getContext());
        if (mCustomer == null)return;
        mHistoryViewModel.requestOrders(mCustomer.getId());
    }

    private void showAlertDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setIcon(R.drawable.ic_warning_24dp)
                .setTitle(R.string.error_title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mHistoryViewModel.requestOrders(mCustomer.getId());
                    }
                });
        if (requireActivity().isFinishing()) {
            return;
        }
        builder.create().show();
    }
}