package com.xilenda.ui.orderHistory;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Israel MEKOMOU [14/07/2020].
 */
public class PaymentDetails implements Parcelable {
    private String method_id;
    private String method_title;
    private boolean paid;

    public PaymentDetails() {
    }

    protected PaymentDetails(Parcel in) {
        method_id = in.readString();
        method_title = in.readString();
        paid = in.readByte() != 0;
    }

    public static final Creator<PaymentDetails> CREATOR = new Creator<PaymentDetails>() {
        @Override
        public PaymentDetails createFromParcel(Parcel in) {
            return new PaymentDetails(in);
        }

        @Override
        public PaymentDetails[] newArray(int size) {
            return new PaymentDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(method_id);
        parcel.writeString(method_title);
        parcel.writeByte((byte) (paid ? 1 : 0));
    }

    public String getMethod_id() {
        return method_id;
    }

    public void setMethod_id(String method_id) {
        this.method_id = method_id;
    }

    public String getMethod_title() {
        return method_title;
    }

    public void setMethod_title(String method_title) {
        this.method_title = method_title;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public String toString() {
        return "PaymentDetails{" +
                "method_id='" + method_id + '\'' +
                ", method_title='" + method_title + '\'' +
                ", paid=" + paid +
                '}';
    }
}
