package com.xilenda.ui.orderHistory;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.xilenda.R;
import com.xilenda.ui.product.Product;
import com.xilenda.utils.OrderStatusUtils;
import com.xilenda.utils.Utils;

import java.util.List;

/**
 * Created by Israel MEKOMOU [04/08/2020].
 */
public class OrderDetailDialog extends AppCompatDialogFragment {
    MutableLiveData<String> productsString = new MutableLiveData<>();
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_order_detail, null);
        builder.setView(v);
        initUi(v);
        return builder.create();
    }

    private void initUi(View v) {
        productsString.setValue("");
        TextView textViewOrderNumber = v.findViewById(R.id.tvOrderNumber);
        TextView textViewOrderDate = v.findViewById(R.id.tvOrderDate);
        TextView textViewOrderStatus = v.findViewById(R.id.tvOrderStatus);
        TextView textViewOrderPrice = v.findViewById(R.id.tvOrderPrice);
        TextView textViewOrderProductCount = v.findViewById(R.id.tvOrderProductCount);
        final TextView textViewOrderProducts = v.findViewById(R.id.tvOrderProduct);
        OrderDetailViewModel viewModel = new ViewModelProvider(this).get(OrderDetailViewModel.class);
        if (getArguments() == null)return;
        final Order order = getArguments().getParcelable("order");
        if (order == null)return;
        textViewOrderNumber.setText(order.getOrder_key());
        textViewOrderDate.setText(order.getDate_created().replace("T", " "));
        textViewOrderStatus.setText(OrderStatusUtils.getHumanStatus(order.getStatus()));
        textViewOrderPrice.setText(order.getTotal().concat(Utils.CURRENCY).concat(" + ").concat(String.valueOf(Utils.DELIVERY_PRICE)).concat(Utils.CURRENCY).concat(getString(R.string.shipping_fee)));
        textViewOrderProductCount.setText(String.valueOf(order.getLine_items().size()));
        for (LineItem item : order.getLine_items()){
            viewModel.getProduct(item.getProduct_id(), getActivity());
        }
        viewModel.getProduct().observe(this, new Observer<Product>() {
            @Override
            public void onChanged(Product product) {
                List<LineItem> items = order.getLine_items();
                for (LineItem item : items){
                    if (item.getProduct_id() == product.getId()){
                        String newString = productsString.getValue();
                        assert newString != null;
                        newString = newString.concat("\u2022 "+getString(R.string.name)+": "+
                                product.getName()+"\n "+getString(R.string.quantity)+": "+
                                item.getQuantity()+"\n "+getString(R.string.price)+": "+
                                item.getPrice().concat(Utils.CURRENCY)+"\n\n"
                        );
                        productsString.setValue(newString);
                    }
                }
            }
        });

        productsString.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null)textViewOrderProducts.setText(s);
            }
        });

    }
}
