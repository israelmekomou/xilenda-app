package com.xilenda.ui.orderHistory;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Israel MEKOMOU [14/07/2020].
 */
public class Order implements Parcelable {
    private int id;
    private int parent_id;
    private String order_key;
    private String number;
    private String key;
    private String created_at;
    private String date_created;
    private String updated_at;
    private String completed_at;
    private String status;
    private String currency;
    private String total;
    private int total_line_items_quantity;
    private String total_tax;
    private String shipping_total;
    private String cart_tax;
    private String shipping_tax;
    private String total_discount;
    private ShippingMethod shipping_methods;
    private PaymentDetails payment_details;
    private Address billing;
    private Address shipping;
    private String customer_note;
    private String customer_ip;
    private String customer_user_agent;
    private int customer_id;
    private String view_order_url;
    private List<LineItem> line_items = new ArrayList<>();

    public Order() {
    }

    protected Order(Parcel in) {
        id = in.readInt();
        order_key = in.readString();
        number = in.readString();
        key = in.readString();
        date_created = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        completed_at = in.readString();
        status = in.readString();
        currency = in.readString();
        total = in.readString();
        total_line_items_quantity = in.readInt();
        total_tax = in.readString();
        shipping_total = in.readString();
        cart_tax = in.readString();
        shipping_tax = in.readString();
        total_discount = in.readString();
        shipping_methods = in.readParcelable(ShippingMethod.class.getClassLoader());
        payment_details = in.readParcelable(PaymentDetails.class.getClassLoader());
        billing = in.readParcelable(Address.class.getClassLoader());
        shipping = in.readParcelable(Address.class.getClassLoader());
        customer_note = in.readString();
        customer_ip = in.readString();
        customer_user_agent = in.readString();
        customer_id = in.readInt();
        view_order_url = in.readString();
        line_items = in.createTypedArrayList(LineItem.CREATOR);
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(order_key);
        parcel.writeString(number);
        parcel.writeString(key);
        parcel.writeString(date_created);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
        parcel.writeString(completed_at);
        parcel.writeString(status);
        parcel.writeString(currency);
        parcel.writeString(total);
        parcel.writeInt(total_line_items_quantity);
        parcel.writeString(total_tax);
        parcel.writeString(shipping_total);
        parcel.writeString(cart_tax);
        parcel.writeString(shipping_tax);
        parcel.writeString(total_discount);
        parcel.writeParcelable(shipping_methods, i);
        parcel.writeParcelable(payment_details, i);
        parcel.writeParcelable(billing, i);
        parcel.writeParcelable(shipping, i);
        parcel.writeString(customer_note);
        parcel.writeString(customer_ip);
        parcel.writeString(customer_user_agent);
        parcel.writeInt(customer_id);
        parcel.writeString(view_order_url);
        parcel.writeTypedList(line_items);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder_key() {
        return order_key;
    }

    public void setOrder_key(String order_key) {
        this.order_key = order_key;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(String completed_at) {
        this.completed_at = completed_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public int getTotal_line_items_quantity() {
        return total_line_items_quantity;
    }

    public void setTotal_line_items_quantity(int total_line_items_quantity) {
        this.total_line_items_quantity = total_line_items_quantity;
    }

    public String getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(String total_tax) {
        this.total_tax = total_tax;
    }

    public String getShipping_total() {
        return shipping_total;
    }

    public void setShipping_total(String shipping_total) {
        this.shipping_total = shipping_total;
    }

    public String getCart_tax() {
        return cart_tax;
    }

    public void setCart_tax(String cart_tax) {
        this.cart_tax = cart_tax;
    }

    public String getShipping_tax() {
        return shipping_tax;
    }

    public void setShipping_tax(String shipping_tax) {
        this.shipping_tax = shipping_tax;
    }

    public String getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(String total_discount) {
        this.total_discount = total_discount;
    }

    public ShippingMethod getShipping_methods() {
        return shipping_methods;
    }

    public void setShipping_methods(ShippingMethod shipping_methods) {
        this.shipping_methods = shipping_methods;
    }

    public PaymentDetails getPayment_details() {
        return payment_details;
    }

    public void setPayment_details(PaymentDetails payment_details) {
        this.payment_details = payment_details;
    }

    public Address getBilling() {
        return billing;
    }

    public void setBilling(Address billing) {
        this.billing = billing;
    }

    public Address getShipping() {
        return shipping;
    }

    public void setShipping(Address shipping) {
        this.shipping = shipping;
    }

    public String getCustomer_note() {
        return customer_note;
    }

    public void setCustomer_note(String customer_note) {
        this.customer_note = customer_note;
    }

    public String getCustomer_ip() {
        return customer_ip;
    }

    public void setCustomer_ip(String customer_ip) {
        this.customer_ip = customer_ip;
    }

    public String getCustomer_user_agent() {
        return customer_user_agent;
    }

    public void setCustomer_user_agent(String customer_user_agent) {
        this.customer_user_agent = customer_user_agent;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getView_order_url() {
        return view_order_url;
    }

    public void setView_order_url(String view_order_url) {
        this.view_order_url = view_order_url;
    }

    public List<LineItem> getLine_items() {
        return line_items;
    }

    public void setLine_items(List<LineItem> line_items) {
        this.line_items = line_items;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", key='" + key + '\'' +
                ", created_at='" + created_at + '\'' +
                ", date_created=" + date_created +
                ", updated_at='" + updated_at + '\'' +
                ", completed_at='" + completed_at + '\'' +
                ", status='" + status + '\'' +
                ", currency='" + currency + '\'' +
                ", total='" + total + '\'' +
                ", total_line_items_quantity=" + total_line_items_quantity +
                ", total_tax='" + total_tax + '\'' +
                ", shipping_total='" + shipping_total + '\'' +
                ", cart_tax='" + cart_tax + '\'' +
                ", shipping_tax='" + shipping_tax + '\'' +
                ", total_discount='" + total_discount + '\'' +
                ", shipping_methods=" + shipping_methods +
                ", payment_details=" + payment_details +
                ", billing=" + billing +
                ", shipping=" + shipping +
                ", customer_note='" + customer_note + '\'' +
                ", customer_ip='" + customer_ip + '\'' +
                ", customer_user_agent='" + customer_user_agent + '\'' +
                ", customer_id=" + customer_id +
                ", view_order_url='" + view_order_url + '\'' +
                ", line_items=" + line_items +
                '}';
    }
}
