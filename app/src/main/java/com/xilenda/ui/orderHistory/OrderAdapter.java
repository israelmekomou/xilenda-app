package com.xilenda.ui.orderHistory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.xilenda.R;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [21/07/2020].
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderViewHolder> {
    private ArrayList<Order> mOrders = new ArrayList<>();
    private FragmentManager mFragmentManager;

    public OrderAdapter(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }

    public void setOrders(ArrayList<Order> orders) {
        mOrders = orders;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_orders, parent, false);
        return new OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        Order order = mOrders.get(position);
        holder.bind(order, mFragmentManager);
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }
}
