package com.xilenda.ui.orderHistory;

import android.app.Activity;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.xilenda.R;
import com.xilenda.ui.product.Product;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailViewModel extends ViewModel {
    private MutableLiveData<Product> mProduct;

    public MutableLiveData<Product> getProduct() {
        return mProduct;
    }

    public OrderDetailViewModel() {
        mProduct = new MutableLiveData<>();
    }

    public void getProduct(int product_id, final Activity activity){
        Call<ResponseBody> call = ApiUtils.getCProductsService().getProduct(product_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.isSuccessful() && response.body() != null){
                    try{
                        String output = response.body().string();
                        Product product = new Gson().fromJson(new JsonParser().parse(output), Product.class);
                        if (product != null){
                            mProduct.setValue(product);
                        }
                    }catch (Exception e){
                        Toasty.info(activity, Utils.API_ERROR_MESSAGE).show();
                    }
                }else {
                    Toasty.info(activity, Utils.API_ERROR_MESSAGE).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                Toasty.info(activity, activity.getString(R.string.no_network_connection)).show();
            }
        });
    }
}
