package com.xilenda.ui.orderHistory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.xilenda.R;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.OrderStatusUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [21/07/2020].
 */
public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView mTextViewOrderDate;
    TextView mTextViewOrderAddress;
    TextView mTextViewOrderPrice;
    TextView mTextViewOrderStatus;
    ImageView mImageViewMore;
    FragmentManager mFragmentManager;
    private Order mOrder;

    public OrderViewHolder(@NonNull View itemView) {
        super(itemView);
        mTextViewOrderDate = itemView.findViewById(R.id.tvOrderDate);
        mTextViewOrderAddress = itemView.findViewById(R.id.tvOrderAddress);
        mTextViewOrderPrice = itemView.findViewById(R.id.tvOrderPrice);
        mTextViewOrderStatus = itemView.findViewById(R.id.tvOrderStatus);
        mImageViewMore = itemView.findViewById(R.id.ivMore);
        mImageViewMore.setOnClickListener(this);
    }

    public void bind(Order order, FragmentManager fragmentManager){
        mOrder = order;
        mFragmentManager = fragmentManager;
        mTextViewOrderDate.setText(mOrder.getDate_created().replace("T", " "));
        mTextViewOrderAddress.setText(mOrder.getShipping().getAddress_1());
        int total = mOrder.getTotal() != null ? Integer.parseInt(mOrder.getTotal()) + Utils.DELIVERY_PRICE : Utils.DELIVERY_PRICE;
        mTextViewOrderPrice.setText(String.valueOf(total).concat(Utils.CURRENCY));
        mTextViewOrderStatus.setText(OrderStatusUtils.getHumanStatus(mOrder.getStatus()));
        mTextViewOrderStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), OrderStatusUtils.getStatusColor(mOrder.getStatus())));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == mImageViewMore.getId()){
            PopupMenu menu = new PopupMenu(view.getContext(), view);
            menu.inflate(R.menu.order_item_menu);
            if (
                    mOrder.getStatus().equals(OrderStatusUtils.CANCELLED_STATUS)  ||
                    mOrder.getStatus().equals(OrderStatusUtils.COMPLETED_STATUS)  ||
                    mOrder.getStatus().equals(OrderStatusUtils.FAILED_STATUS)     ||
                    mOrder.getStatus().equals(OrderStatusUtils.REFUNDED_STATUS)
            )menu.getMenu().findItem(R.id.item_cancel).setVisible(false);
            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    switch (menuItem.getItemId()){
                        case R.id.item_details :
                            showDetails();
                            return true;
                        case R.id.item_cancel :
                            cancelOrder();
                            return true;
                        default:
                            return false;
                    }
                }
            });
            menu.show();
        }
    }

    private void cancelOrder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext())
                .setIcon(R.drawable.ic_warning_24dp)
                .setTitle(R.string.title_warning)
                .setMessage(R.string.want_to_cancel_order)
                .setPositiveButton(itemView.getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Call<ResponseBody> call = ApiUtils.getOrderApi().cancelOrder(mOrder.getId(), "cancelled");
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                                if (response.isSuccessful() && response.body() != null){
                                    Snackbar.make(itemView, R.string.order_cancelled, 5000).show();
                                    mTextViewOrderStatus.setText(OrderStatusUtils.getHumanStatus(OrderStatusUtils.CANCELLED_STATUS));
                                    mTextViewOrderStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), OrderStatusUtils.getStatusColor(OrderStatusUtils.CANCELLED_STATUS)));
                                }else {
                                    Toasty.info(itemView.getContext(), Utils.API_ERROR_MESSAGE).show();
                                }
                            }

                            @Override
                            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                                Toasty.info(itemView.getContext(), Utils.ON_FAILURE_MESSAGE).show();
                            }
                        });
                    }
                })
                .setNeutralButton(itemView.getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.create().show();
    }

    private void showDetails() {
        OrderDetailDialog detailDialog = new OrderDetailDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("order", mOrder);
        detailDialog.setArguments(bundle);
        detailDialog.show(mFragmentManager, "order_detail");
    }
}