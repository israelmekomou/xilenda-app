package com.xilenda.ui.authentication.login;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.xilenda.BaseActivity;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.ui.authentication.forgot_password.request_reset_code.RequestResetCodeActivity;
import com.xilenda.ui.authentication.register.RegisterActivity;
import com.xilenda.uiComponents.Utilities;

/**
 * Created by Israel MEKOMOU [15/07/2020].
 */
public class LoginActivity extends BaseActivity {
    private TextInputEditText mEditTextLogin;
    private TextInputEditText mEditTextPassword;
    private TextView mTextViewForgotPassword;
    private Button mButtonLogin;
    private CardView mCardViewSkip;
    private ProgressBar mProgressBar;
    private LoginViewModel mViewModel;
    private boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUi();
    }

    private void initUi() {
        mViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        mEditTextLogin = findViewById(R.id.tietLogin);
        mEditTextPassword = findViewById(R.id.tietPassword);
        mProgressBar = findViewById(R.id.progressBar);
        mTextViewForgotPassword = findViewById(R.id.tvForgotPassword);
        mButtonLogin = findViewById(R.id.btnLogin);
        mCardViewSkip = findViewById(R.id.cvSkip);

        mViewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                assert s != null;
                Utilities.showAlertDialog(LoginActivity.this, R.drawable.ic_warning_24dp,
                        getString(R.string.error_title), s, true);
            }
        });

        mViewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                loading = aBoolean;
                mProgressBar.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                mEditTextLogin.setEnabled(!aBoolean);
                mEditTextPassword.setEnabled(!aBoolean);
                mTextViewForgotPassword.setEnabled(!aBoolean);
                mButtonLogin.setEnabled(!aBoolean);
                mButtonLogin.setBackgroundColor(
                        aBoolean ? ContextCompat.getColor(LoginActivity.this, R.color.grey_40) :
                                ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary)
                );
                mCardViewSkip.setEnabled(!aBoolean);

            }
        });
    }

    public void login(View view) {
        switch (view.getId()){
            case R.id.btnLogin :
                if (verifyData()){
                    mViewModel.authenticateUser(this,
                            mEditTextLogin.getText().toString(),
                            mEditTextPassword.getText().toString()
                    );
                }
                break;
            case R.id.tvForgotPassword :
                Utilities.openActivity(this, RequestResetCodeActivity.class.getName());
                break;
            case R.id.tvSignIn :
                Utilities.openActivity(this, RegisterActivity.class.getName());
                finish();
                break;
            case R.id.cvSkip :
                Utilities.openActivity(this, MainActivity.class.getName());
                break;
        }
    }

    private boolean verifyData() {
        boolean isOk = true;
        if (TextUtils.isEmpty(mEditTextLogin.getText())){
            isOk = false;
            mEditTextLogin.setError(getString(R.string.field_is_mandatory));
        }else {
            mEditTextLogin.setError(null);
        }
        if (TextUtils.isEmpty(mEditTextPassword.getText())){
            isOk = false;
            mEditTextPassword.setError(getString(R.string.field_is_mandatory));
        }else {
            if (mEditTextPassword.getText().toString().length() < 6){
                isOk = false;
                mEditTextPassword.setError(getString(R.string.min_length_password));
            }else {
                mEditTextPassword.setError(null);
            }
        }
        return isOk;
    }

    @Override
    public void onBackPressed() {
        if (loading) return;
        super.onBackPressed();
    }
}