package com.xilenda.ui.authentication.forgot_password.request_reset_code;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.xilenda.R;
import com.xilenda.ui.authentication.forgot_password.reset_password.ResetPasswordActivity;
import com.xilenda.ui.authentication.register.RegisterActivity;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [04/08/2020].
 */
public class RequestResetCodeViewModel extends ViewModel {
    public static final String EMAIL_EXTRA = "email_extra";
    private MutableLiveData<String> mError;
    private MutableLiveData<Boolean> mLoad;

    public RequestResetCodeViewModel() {
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public void requestResetCode(final Activity activity, final String email){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getAuthenticationService().requestResetCode(email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try{
                        String output = response.body().string();
                        JSONObject object = new JSONObject(output);
                        if (object.getJSONObject("data").getInt("status") == 200){
                            showSuccessDialog(activity, email);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    if (response.errorBody() != null){
                        try{
                            String output = response.errorBody().string();
                            JSONObject object = new JSONObject(output);
                            if (object.getString("code").equals("bad_email")){
                                showErrorDialog(activity, email);
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            mError.setValue(Utils.API_ERROR_MESSAGE);
                        }
                    }else {
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mLoad.setValue(false);
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
            }
        });
    }

    private void showSuccessDialog(final Activity activity, final String email) {
        AlertDialog.Builder builder = new  AlertDialog.Builder(activity)
                .setIcon(R.drawable.logo)
                .setCancelable(false)
                .setTitle("Votre code de récupération a été envoyé")
                .setMessage("Veuillez vérifier votre boite mail et vos spams. Si vous n'avez pas reçu de code, veuillez réessayer ou contactez nous.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(activity, ResetPasswordActivity.class);
                        intent.putExtra(EMAIL_EXTRA, email);
                        activity.startActivity(intent);
                        dialogInterface.dismiss();
                        activity.finish();
                    }
                });
        if (!activity.isFinishing())builder.create().show();
    }

    private void showErrorDialog(final Activity activity, String email) {
        AlertDialog.Builder builder = new  AlertDialog.Builder(activity)
                .setIcon(R.drawable.ic_warning_24dp)
                .setCancelable(false)
                .setTitle(R.string.title_warning)
                .setMessage(activity.getString(R.string.no_email_found_1)+ email + activity.getString(R.string.no_email_found_2))
                .setPositiveButton(R.string.sign_in, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Utilities.openActivity(activity, RegisterActivity.class.getName());
                        dialogInterface.dismiss();
                        activity.finish();
                    }
                })
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        if (!activity.isFinishing())builder.create().show();
    }
}
