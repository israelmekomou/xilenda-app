package com.xilenda.ui.authentication.forgot_password.reset_password;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputEditText;
import com.xilenda.R;
import com.xilenda.ui.authentication.forgot_password.request_reset_code.RequestResetCodeViewModel;
import com.xilenda.uiComponents.Utilities;

/**
 * Created by Israel MEKOMOU [04/08/2020].
 */
public class ResetPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initUi();
    }

    private void initUi() {
        final TextInputEditText editTextEmail = findViewById(R.id.etEmail);
        final TextInputEditText editTextCode = findViewById(R.id.etCode);
        final TextInputEditText editTextPassword = findViewById(R.id.etPassword);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        final Button button = findViewById(R.id.btnSave);

        final ResetPasswordViewModel viewModel = new ViewModelProvider(this).get(ResetPasswordViewModel.class);

        if (getIntent() != null && getIntent().hasExtra(RequestResetCodeViewModel.EMAIL_EXTRA)){
            editTextEmail.setText(getIntent().getStringExtra(RequestResetCodeViewModel.EMAIL_EXTRA));
        }
        viewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s == null || s.isEmpty())return;
                Utilities.showAlertDialog(ResetPasswordActivity.this, R.drawable.ic_warning_24dp, getString(R.string.title_warning), s, true);
            }
        });

        viewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                progressBar.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                editTextEmail.setEnabled(!aBoolean);
                editTextCode.setEnabled(!aBoolean);
                editTextPassword.setEnabled(!aBoolean);
                button.setEnabled(!aBoolean);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (verify(editTextEmail, editTextCode, editTextPassword)){
                    viewModel.resetPassword(ResetPasswordActivity.this, editTextEmail.getText().toString(), editTextCode.getText().toString(), editTextPassword.getText().toString());
                }
            }
        });

    }

    private boolean verify(TextInputEditText editTextEmail, TextInputEditText editTextCode, TextInputEditText editTextPassword) {
        boolean juge = true;
        if (TextUtils.isEmpty(editTextEmail.getText())){
            editTextEmail.setError(getString(R.string.field_is_mandatory));
            juge = false;
        }else {
            if (!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
                editTextEmail.setError(getString(R.string.invalid_email_address));
                juge = false;
            }else {
                editTextEmail.setError(null);
            }
        }
        if (TextUtils.isEmpty(editTextCode.getText())){
            editTextCode.setError(getString(R.string.field_is_mandatory));
            juge = false;
        }else {
            editTextCode.setError(null);
        }
        if (TextUtils.isEmpty(editTextPassword.getText())){
            juge = false;
            editTextPassword.setError(getString(R.string.field_is_mandatory));
        }else {
            if (editTextPassword.getText().toString().length() < 6){
                juge = false;
                editTextPassword.setError(getString(R.string.invalid_password));
            }else {
                editTextPassword.setError(null);
            }
        }
        return juge;
    }


}