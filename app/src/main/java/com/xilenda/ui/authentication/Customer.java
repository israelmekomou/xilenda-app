package com.xilenda.ui.authentication;

import android.os.Parcel;
import android.os.Parcelable;

import com.xilenda.ui.orderHistory.Address;

/**
 * Created by Israel MEKOMOU [15/07/2020].
 */
public class Customer implements Parcelable{
    private int id;
    private String date_created;
    private String email;
    private String first_name;
    private String last_name;
    private String role;
    private String username;
    private Address billing;
    private Address shipping;

    public Customer() {
    }

    protected Customer(Parcel in) {
        id = in.readInt();
        date_created = in.readString();
        email = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        role = in.readString();
        username = in.readString();
        billing = in.readParcelable(Address.class.getClassLoader());
        shipping = in.readParcelable(Address.class.getClassLoader());
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Address getBilling() {
        return billing;
    }

    public void setBilling(Address billing) {
        this.billing = billing;
    }

    public Address getShipping() {
        return shipping;
    }

    public void setShipping(Address shipping) {
        this.shipping = shipping;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(date_created);
        parcel.writeString(email);
        parcel.writeString(first_name);
        parcel.writeString(last_name);
        parcel.writeString(role);
        parcel.writeString(username);
        parcel.writeParcelable(billing, i);
        parcel.writeParcelable(shipping, i);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", date_created='" + date_created + '\'' +
                ", email='" + email + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", role='" + role + '\'' +
                ", username='" + username + '\'' +
                ", billing=" + billing +
                ", shipping=" + shipping +
                '}';
    }
}
