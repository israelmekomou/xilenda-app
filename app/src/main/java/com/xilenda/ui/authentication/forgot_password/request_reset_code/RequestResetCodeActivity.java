package com.xilenda.ui.authentication.forgot_password.request_reset_code;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputEditText;
import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.uiComponents.Utilities;

/**
 * Created by Israel MEKOMOU [04/08/2020].
 */
public class RequestResetCodeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_reset_code);
        initUi();
    }

    private void initUi() {
        final TextInputEditText editTextEmail = findViewById(R.id.etEmail);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        final Button buttonSend = findViewById(R.id.btnSend);
        final RequestResetCodeViewModel viewModel = new ViewModelProvider(this).get(RequestResetCodeViewModel.class);

        viewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s == null || s.isEmpty())return;
                Utilities.showAlertDialog(RequestResetCodeActivity.this, R.drawable.ic_warning_24dp, getString(R.string.title_warning), s, true);
            }
        });

        viewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                progressBar.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                editTextEmail.setEnabled(!aBoolean);
                buttonSend.setEnabled(!aBoolean);
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editTextEmail.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    editTextEmail.setError(getString(R.string.field_is_mandatory));
                } else {
                    if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        editTextEmail.setError(null);
                        viewModel.requestResetCode(RequestResetCodeActivity.this, email);
                    } else {
                        editTextEmail.setError(getString(R.string.invalid_email_address));
                    }
                }
            }
        });
    }
}