package com.xilenda.ui.authentication.forgot_password.reset_password;

import android.app.Activity;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.xilenda.R;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [04/08/2020].
 */
public class ResetPasswordViewModel extends ViewModel {
    private MutableLiveData<String> mError;
    private MutableLiveData<Boolean> mLoad;

    public ResetPasswordViewModel() {
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public void resetPassword(final Activity activity, final String email, String code, String password){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getAuthenticationService().resetPassword(email, code, password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String output = response.body().string();
                        JSONObject object = new JSONObject(output);
                        if (object.getJSONObject("data").getInt("status") == 200){
                            Toasty.success(activity, activity.getString(R.string.your_password_is_reset)).show();
                            activity.onBackPressed();
                        }else {
                            mError.setValue(Utils.API_ERROR_MESSAGE);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    if (response.errorBody() != null){
                        try {
                            String output = response.errorBody().string();
                            JSONObject object = new JSONObject(output);
                            if (object.getString("code").equals("bad_email")){
                                Utilities.showAlertDialog(
                                        activity,
                                        R.drawable.ic_warning_24dp,
                                        activity.getString(R.string.title_warning),
                                        activity.getString(R.string.no_email_found_1)+ email + activity.getString(R.string.no_email_found_2),
                                        true);
                            }else if (object.getString("code").equals("bad_request")){
                                Utilities.showAlertDialog(
                                        activity,
                                        R.drawable.ic_warning_24dp,
                                        activity.getString(R.string.title_warning),
                                        object.getString("message"),
                                        true
                                );
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            mError.setValue(Utils.API_ERROR_MESSAGE);
                        }
                    }else {
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mLoad.setValue(false);
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
            }
        });
    }
}
