package com.xilenda.ui.authentication.register;

import android.app.Activity;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.xilenda.MainActivity;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [15/07/2020].
 */
public class RegisterViewModel extends ViewModel {
    private MutableLiveData<String> mError;
    private MutableLiveData<Boolean> mLoad;

    public RegisterViewModel(){
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public void registerUser(final Activity activity, String displayName, String email, String username, String password){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getAuthenticationService().register(
//                Utils.WEB_HOOK_NAME,
//                Utils.WEB_HOOK_API_KEY,
//                Utils.WEB_HOOK_ACTION,
                Utils.CONSUMER_KEY,
                Utils.CONSUMER_SECRETE,
                email,
                displayName,
                username,
                password
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String output = response.body().string();
                        Customer customer = new Gson().fromJson(new JsonParser().parse(output), Customer.class);
                        CustomerHelper.setCurrentCustomer(activity, customer);
                        Utilities.openActivity(activity, MainActivity.class.getName());
                    } catch (IOException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    if (response.errorBody() != null){
                        try{
                            String output = response.errorBody().string();
                            JSONObject object = new JSONObject(output);
                            mError.setValue(object.getString("message"));
                        }catch (IOException | JSONException e) {
                            e.printStackTrace();
                            mError.setValue(Utils.API_ERROR_MESSAGE);
                        }
                    }else {
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
                mLoad.setValue(false);
            }
        });
    }
}
