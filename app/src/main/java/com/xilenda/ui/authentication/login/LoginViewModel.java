package com.xilenda.ui.authentication.login;

import android.app.Activity;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.core.CustomerHelper;
import com.xilenda.ui.authentication.Customer;
import com.xilenda.uiComponents.Utilities;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.PreferenceUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.util.Patterns.EMAIL_ADDRESS;

public class LoginViewModel extends ViewModel {
    private MutableLiveData<String> mError;
    private MutableLiveData<Boolean> mLoad;

    public LoginViewModel() {
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public void authenticateUser(final Activity activity, final String login, final String password){
        mLoad.setValue(true);
        Call<ResponseBody> call = isValidEmail(login) ?
                ApiUtils.getAuthenticationService().loginWithEmail(login, password) :
                ApiUtils.getAuthenticationService().loginWithUsername(login, password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try{
                        String output = response.body().string();
                        JSONObject object = new JSONObject(output);
                        if (object.getString("status").equals("ok")){
                            JSONObject userObject = object.getJSONObject("user");
                            Customer customer = new Gson().fromJson(new JsonParser().parse(userObject.toString()), Customer.class);
                            getUserInfo(activity, customer.getId());
                            Log.e("CUSTOMER", customer.toString());
                        }else {
                            mError.setValue(object.getString("error"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    mError.setValue(activity.getString(R.string.verify_info));
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mLoad.setValue(false);
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
            }
        });
    }

    public void getUserInfo(final Activity activity, final int id){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getAuthenticationService().getUserInfo(id, Utils.CONSUMER_KEY, Utils.CONSUMER_SECRETE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String output = response.body().string();
                        Customer customer = new Gson().fromJson(new JsonParser().parse(output), Customer.class);
                        CustomerHelper.setCurrentCustomer(activity, customer);
                        PreferenceUtils.setInt(activity, Utils.CUSTOMER_ID, id);
//                        PreferenceUtils.setString(activity, Utils.PASSWORD_KEY, password);
                        Utilities.openActivity(activity, MainActivity.class.getName());
                        Toasty.success(activity, "Bienvenue.").show();
                    } catch (IOException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    mError.setValue(Utils.ON_FAILURE_MESSAGE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mLoad.setValue(false);
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
            }
        });
    }

    private boolean isValidEmail(String email){
        return EMAIL_ADDRESS.matcher(email).matches();
    }
}
