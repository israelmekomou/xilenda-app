package com.xilenda.ui.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.xilenda.BaseActivity;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.ui.authentication.login.LoginActivity;
import com.xilenda.ui.authentication.register.RegisterActivity;
import com.xilenda.uiComponents.Utilities;

/**
 * Created by Israel MEKOMOU [15/07/2020].
 */
public class AuthActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
    }

    public void authenticate(View view) {
        switch (view.getId()){
            case R.id.btnLogin :
                Utilities.openActivity(this, LoginActivity.class.getName());
                break;
            case R.id.btnSignIn :
                Utilities.openActivity(this, RegisterActivity.class.getName());
                break;
            case R.id.cvSkip :
                Utilities.openActivity(this, MainActivity.class.getName());
                break;
            default:
                break;
        }
    }
}