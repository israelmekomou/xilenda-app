package com.xilenda.ui.authentication.register;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputEditText;
import com.xilenda.BaseActivity;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.ui.authentication.login.LoginActivity;
import com.xilenda.uiComponents.Utilities;

import static android.util.Patterns.EMAIL_ADDRESS;

/**
 * Created by Israel MEKOMOU [15/07/2020].
 */
public class RegisterActivity extends BaseActivity {
    private TextInputEditText mEditTextName;
    private TextInputEditText mEditTextUsername;
    private TextInputEditText mEditTextEmail;
    private TextInputEditText mEditTextPassword;
    private TextInputEditText mEditTextConfirmPassword;
    private Button mButtonSignIn;
    private CardView mCardViewSkip;
    private ProgressBar mProgressBar;
    private boolean loading = false;
    private RegisterViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Utilities.hideKeyBoard(this);
        initUi();
    }

    private void initUi() {
        mViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        mEditTextName = findViewById(R.id.tietUserFullName);
        mEditTextUsername = findViewById(R.id.tietUsername);
        mEditTextEmail = findViewById(R.id.tietUserEmail);
        mEditTextPassword = findViewById(R.id.tietUserPassword);
        mEditTextConfirmPassword = findViewById(R.id.tietUserPasswordConfirm);
        mProgressBar = findViewById(R.id.progressBar);
        mCardViewSkip = findViewById(R.id.cvSkip);
        mButtonSignIn = findViewById(R.id.btnSignIn);

        mViewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                assert s != null;
                Utilities.showAlertDialog(RegisterActivity.this, R.drawable.ic_warning_24dp,
                        getString(R.string.error_title), s, true);
            }
        });

        mViewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                loading = aBoolean;
                mProgressBar.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                mCardViewSkip.setEnabled(!aBoolean);
                mButtonSignIn.setEnabled(!aBoolean);
                mButtonSignIn.setBackgroundColor(
                        aBoolean ? ContextCompat.getColor(RegisterActivity.this, R.color.grey_40) :
                                ContextCompat.getColor(RegisterActivity.this, R.color.colorPrimary)
                );
                mEditTextName.setEnabled(!aBoolean);
                mEditTextUsername.setEnabled(!aBoolean);
                mEditTextEmail.setEnabled(!aBoolean);
                mEditTextPassword.setEnabled(!aBoolean);
                mEditTextConfirmPassword.setEnabled(!aBoolean);
            }
        });
    }

    public void register(View view) {
        switch (view.getId()){
            case R.id.cvSkip :
                Utilities.openActivity(this, MainActivity.class.getName());
                break;
            case R.id.tvLogin :
                Utilities.openActivity(this, LoginActivity.class.getName());
                finish();
                break;
            case R.id.btnSignIn :
                if (verifyData()){
                    mViewModel.registerUser(this,
                            mEditTextName.getText().toString(),
                            mEditTextEmail.getText().toString(),
                            mEditTextUsername.getText().toString(),
                            mEditTextPassword.getText().toString()
                    );
                }
                break;
        }
    }

    private boolean verifyData() {
        boolean isOk = true;
        if (TextUtils.isEmpty(mEditTextName.getText())){
            isOk = false;
            mEditTextName.setError(getString(R.string.field_is_mandatory));
        }else {
            mEditTextName.setError(null);
        }
        if (TextUtils.isEmpty(mEditTextUsername.getText())){
            isOk = false;
            mEditTextUsername.setError(getString(R.string.field_is_mandatory));
        }else {
            mEditTextUsername.setError(null);
        }
        if (TextUtils.isEmpty(mEditTextEmail.getText())){
            isOk = false;
            mEditTextEmail.setError(getString(R.string.field_is_mandatory));
        }else {
            if (!isValidEmail(mEditTextEmail.getText().toString())){
                isOk = false;
                mEditTextEmail.setError(getString(R.string.invalid_email_address));
            }else {
                mEditTextEmail.setError(null);
            }
        }
        if (TextUtils.isEmpty(mEditTextPassword.getText())){
            isOk = false;
            mEditTextPassword.setError(getString(R.string.field_is_mandatory));
        }else {
            if (mEditTextPassword.getText().toString().length() < 6){
                isOk = false;
                mEditTextPassword.setError(getString(R.string.invalid_password));
            }else {
                mEditTextPassword.setError(null);
            }
        }
        if (TextUtils.isEmpty(mEditTextConfirmPassword.getText())){
            isOk = false;
            mEditTextConfirmPassword.setError(getString(R.string.field_is_mandatory));
        }else {
            if (!mEditTextPassword.getText().toString().equals(mEditTextConfirmPassword.getText().toString())){
                isOk = false;
                mEditTextConfirmPassword.setError(getString(R.string.invalid_password_confirm));
            }
            mEditTextConfirmPassword.setError(null);
        }
        Log.e("ISOK", String.valueOf(isOk));
        return isOk;
    }

    private boolean isValidEmail(String email){
        return EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        if (loading)return;
        super.onBackPressed();
    }
}