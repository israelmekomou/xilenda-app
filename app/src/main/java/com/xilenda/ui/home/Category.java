package com.xilenda.ui.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.xilenda.ui.product.Image;
import com.xilenda.ui.product.Product;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [10/07/2020].
 */
public class Category implements Parcelable {
    private int id;
    private int parent;
    private int menu_order;
    private int count;
    private String name;
    private String slug;
    private String description;
    private String display;
    private String image;
    private ArrayList<Product> products;

    public Category() {
    }

    protected Category(Parcel in) {
        id = in.readInt();
        parent = in.readInt();
        menu_order = in.readInt();
        count = in.readInt();
        name = in.readString();
        slug = in.readString();
        description = in.readString();
        display = in.readString();
        image = in.readString();
        products = in.createTypedArrayList(Product.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(parent);
        dest.writeInt(menu_order);
        dest.writeInt(count);
        dest.writeString(name);
        dest.writeString(slug);
        dest.writeString(description);
        dest.writeString(display);
        dest.writeString(image);
        dest.writeTypedList(products);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", parent=" + parent +
                ", menu_order=" + menu_order +
                ", count=" + count +
                ", name='" + name + '\'' +
                ", slug='" + slug + '\'' +
                ", description='" + description + '\'' +
                ", display='" + display + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getMenu_order() {
        return menu_order;
    }

    public void setMenu_order(int menu_order) {
        this.menu_order = menu_order;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
