package com.xilenda.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.xilenda.R;
import com.xilenda.utils.PreferenceUtils;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class HomeFragment extends Fragment {

    public static final String CATEGORIES = "categories";
    private HomeViewModel mHomeViewModel;
    private CategoryAdapter mAdapter;
    private ArrayList<Category> mCategories;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        if (!PreferenceUtils.getString(getContext(), CATEGORIES).equals("")){
            mCategories = new Gson().fromJson(PreferenceUtils.getString(getContext(), CATEGORIES), new TypeToken<ArrayList<Category>>(){}.getType());
        }
        initUi(root);
//        initSlider(root);
        return root;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mCategories == null)return;
        if (!mCategories.isEmpty()){
            outState.putParcelableArrayList(CATEGORIES, mCategories);
        }
    }

    private void initSlider(View root) {
        ViewPager pager = root.findViewById(R.id.viewPager);
        LinearLayout layout = root.findViewById(R.id.lyt_dots);
        ArrayList<String> urls = new ArrayList<>();
        urls.add("https://s.ftcdn.net/v2013/pics/all/curated/RKyaEDwp8J7JKeZWQPuOVWvkUjGQfpCx_cover_580.jpg?r=1a0fc22192d0c808b8bb2b9bcfbf4a45b1793687");
        urls.add("https://blog-fr.orson.io/wp-content/uploads/2017/06/jpeg-ou-png.jpg");

        SliderHelper sliderHelper = new SliderHelper(pager, layout, urls);
        sliderHelper.initViewPager();
    }

    private void initUi(View root) {
        mAdapter = new CategoryAdapter();
        RecyclerView recyclerViewCategory = root.findViewById(R.id.rvCategory);
        recyclerViewCategory.setAdapter(mAdapter);
        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(getContext()));

        mHomeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        if (mCategories == null || mCategories.isEmpty()){
            mHomeViewModel.requestCategories();
        }else {
            mAdapter.setCategories(mCategories);
        }
        mHomeViewModel.getCategories().observe(getViewLifecycleOwner(), new Observer<ArrayList<Category>>() {
            @Override
            public void onChanged(ArrayList<Category> categories) {
                if (categories != null){
                    mCategories = categories;
                    mAdapter.setCategories(mCategories);
                    JsonArray array = new Gson().toJsonTree(categories, new TypeToken<ArrayList<Category>>(){}.getType()).getAsJsonArray();
                    PreferenceUtils.setString(getContext(), CATEGORIES, array.toString());
                }
            }
        });

        mHomeViewModel.getError().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null && !TextUtils.isEmpty(s)){
                    showAlertDialog(s);
                }
            }
        });
    }

    private void showAlertDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setIcon(R.drawable.ic_warning_24dp)
                .setTitle(R.string.error_title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mHomeViewModel.requestCategories();
                    }
                });
        if (requireActivity().isFinishing()) {
            return;
        }
        builder.create().show();
    }
}