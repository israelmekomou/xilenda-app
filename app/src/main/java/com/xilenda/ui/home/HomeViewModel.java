package com.xilenda.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class HomeViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Category>> mCategories;
    private MutableLiveData<String> mError ;

    public HomeViewModel() {
       mCategories = new MutableLiveData<>();
       mError = new MutableLiveData<>();
    }

    public LiveData<ArrayList<Category>> getCategories(){
        return mCategories;
    }

    public LiveData<String> getError(){
        return mError;
    }

    public void requestCategories(){
        Call<ResponseBody> call = ApiUtils.getProductsService().getAllCategories(
                Utils.CONSUMER_KEY,
                Utils.CONSUMER_SECRETE,
                -1
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String responseString = response.body().string();
                        JSONObject object = new JSONObject(responseString);
                        JSONArray categoryArray = object.getJSONArray("product_categories");
                        ArrayList<Category> categories = new Gson().fromJson(categoryArray.toString(), new TypeToken<ArrayList<Category>>(){}.getType());
                        mCategories.setValue(categories);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    mError.setValue(Utils.API_ERROR_MESSAGE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                t.printStackTrace();
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
            }
        });
    }
}