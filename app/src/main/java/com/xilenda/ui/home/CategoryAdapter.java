package com.xilenda.ui.home;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xilenda.R;
import com.xilenda.ui.subcategoryProducts.SubCategoryAdapter;
import com.xilenda.ui.subcategoryProducts.SubcategoryActivity;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [10/07/2020].
 */
public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String CATEGORY = "category";
    public static final String SUBCATEGORIES = "subcategories";
    public static final int ONE_COLUMN_ROW_TYPE = 1;
    public static final int TWO_COLUMN_ROW_TYPE = 2;
    ArrayList<Category> mCategories = new ArrayList<>();
    ArrayList<Category> mAllCategories = new ArrayList<>();

    public void setCategories(ArrayList<Category> categories) {
        for (Category c : categories) {
            if (c.getParent() == 0)mCategories.add(c);
        }
        mAllCategories = categories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
//        Log.e("VIEWTYPE", String.valueOf(viewType));
        View itemView = new View(parent.getContext());
        if (viewType == ONE_COLUMN_ROW_TYPE){
            itemView = LayoutInflater.from(context)
                    .inflate(R.layout.item_category_one_column, parent, false);
            return new CategoryViewHolderOneColumn(itemView);
        }else if (viewType == TWO_COLUMN_ROW_TYPE){
            itemView = LayoutInflater.from(context)
                    .inflate(R.layout.item_category_two_columns, parent, false);
            return new CategoryViewHolderTwoColumns(itemView);
        }
        return new RecyclerView.ViewHolder(itemView) {};
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CategoryViewHolderOneColumn){
            int i = getIndex(ONE_COLUMN_ROW_TYPE, position);
            Category category = mCategories.get(i);
            ((CategoryViewHolderOneColumn) (holder)).bind(category);
        }else {
            int i = getIndex(TWO_COLUMN_ROW_TYPE, position);
            Category category1 = mCategories.get(i + 1);
            Category category2 = mCategories.get(i + 2);

            ((CategoryViewHolderTwoColumns) (holder)).bind(category1,
                    ((CategoryViewHolderTwoColumns) holder).mTextViewCategoryName,
                    ((CategoryViewHolderTwoColumns) holder).mImageViewCategory);
            ((CategoryViewHolderTwoColumns) (holder)).bind(category2,
                    ((CategoryViewHolderTwoColumns) holder).mTextViewCategoryName2,
                    ((CategoryViewHolderTwoColumns) holder).mImageViewCategory2);
        }
    }

    @Override
    public int getItemCount() {
        int[] item = new int[110];
        int j = 1;
        for (int i = 1; i < 100; i += 3){
            item[i] = j++;
            item[i + 1] = item[i + 2] = j++;
        }
        return item[mCategories.size()];
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return ONE_COLUMN_ROW_TYPE;
        } else {
            return TWO_COLUMN_ROW_TYPE;
        }
    }

    public class CategoryViewHolderOneColumn extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImageViewCategory;
        TextView mTextViewCategoryName;

        public CategoryViewHolderOneColumn(@NonNull View itemView) {
            super(itemView);
            mImageViewCategory = itemView.findViewById(R.id.ivCategory);
            mTextViewCategoryName = itemView.findViewById(R.id.tvCategoryName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getIndex(ONE_COLUMN_ROW_TYPE, getAdapterPosition());
            Category category = mCategories.get(position);
            Intent intent = new Intent(view.getContext(), SubcategoryActivity.class);
            intent.putExtra(SubCategoryAdapter.CATEGORY, category.getName());
            intent.putParcelableArrayListExtra(SUBCATEGORIES, getSubCategories(category.getId()));
            view.getContext().startActivity(intent);

        }

        private ArrayList<Category> getSubCategories(int parentId){
            ArrayList<Category> categories = new ArrayList<>();
            for (Category c : mAllCategories){
                if (c.getParent() == parentId)categories.add(c);
            }
            return categories;
        }

        private void bind(Category category) {
            mTextViewCategoryName.setText(category.getName());

            if (category.getImage() != null &&
                    !TextUtils.isEmpty(category.getImage())){
                Glide.with(itemView.getContext())
                        .load(category.getImage())
                        .into(mImageViewCategory);
            }
        }
    }

    public class CategoryViewHolderTwoColumns extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImageViewCategory;
        TextView mTextViewCategoryName;
        ImageView mImageViewCategory2;
        TextView mTextViewCategoryName2;
        CardView mCardViewLeft;
        CardView mCardViewRight;

        public CategoryViewHolderTwoColumns(@NonNull View itemView) {
            super(itemView);
            mImageViewCategory = itemView.findViewById(R.id.ivCategory);
            mTextViewCategoryName = itemView.findViewById(R.id.tvCategoryName);
            mImageViewCategory2 = itemView.findViewById(R.id.ivCategory2);
            mTextViewCategoryName2 = itemView.findViewById(R.id.tvCategoryName2);
            mCardViewLeft = itemView.findViewById(R.id.cvLeft);
            mCardViewRight = itemView.findViewById(R.id.cvRight);
            mCardViewLeft.setOnClickListener(this);
            mCardViewRight.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position;
            if(view.getId() == R.id.cvLeft){
                position = getIndex2(getAdapterPosition(), true);
            }else {
                position = getIndex2(getAdapterPosition(), false);
            }

            Category category = mCategories.get(position);
            Intent intent = new Intent(view.getContext(), SubcategoryActivity.class);
            intent.putExtra(SubCategoryAdapter.CATEGORY, category.getName());
            intent.putParcelableArrayListExtra(SUBCATEGORIES, getSubCategories(category.getId()));
            view.getContext().startActivity(intent);
        }

        private ArrayList<Category> getSubCategories(int parentId){
            ArrayList<Category> categories = new ArrayList<>();
            for (Category c : mAllCategories){
                if (c.getParent() == parentId)categories.add(c);
            }
            return categories;
        }

        private void bind(Category category, TextView textViewName, ImageView imageView) {
            textViewName.setText(category.getName());

            if (category.getImage() != null &&
                    !TextUtils.isEmpty(category.getImage())){
                Glide.with(itemView.getContext())
                        .load(category.getImage())
                        .into(imageView);
            }
        }
    }

    private int getIndex(int viewType, int position) {
        if (viewType == ONE_COLUMN_ROW_TYPE) {
            return (position * 3) / 2;
        } else {
            return ((position - 1) / 2) * 3;
        }
    }

    private int getIndex2(int position, boolean left) {
        int index;
        index = ((int) position / 2) + position;
        if (left) return index;
        return index + 1;
    }
}
