package com.xilenda.ui.home;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.xilenda.R;

import java.util.ArrayList;


/**
 * Created by Israel MEKOMOU [09/07/2020].
 */
public class ImageSliderAdapter extends PagerAdapter {
    private ArrayList<String> mUrls;

    public ImageSliderAdapter(ArrayList<String> urls) {
        mUrls = urls;
    }

    @Override
    public int getCount() {
        return mUrls.size();
    }

    public void setItems(ArrayList<String> urls){
        this.mUrls = urls;
        notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View v = inflater.inflate(R.layout.item_home_image_slide, container, false);
        ImageView imageView = v.findViewById(R.id.ivHomeSlider);
        if (mUrls.get(position) != null){
            Glide.with(container.getContext())
                    .load(mUrls.get(position))
                    .into(imageView);
            container.addView(v);
        }

        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView(((RelativeLayout) object));
    }
}
