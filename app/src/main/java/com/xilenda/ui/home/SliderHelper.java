package com.xilenda.ui.home;

import android.app.Activity;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.ViewPager;

import com.xilenda.R;

import java.util.ArrayList;

/**
 * Created by Israel MEKOMOU [09/07/2020].
 */
public class SliderHelper {
    public static final int SLIDE_DELAY = 10000;
    private ArrayList<String> mUrls;
    private ViewPager mViewPager;
    private LinearLayout mLinearLayout;
    private Runnable runnable;
    private Handler handler  = new Handler();
    private ImageSliderAdapter mSliderAdapter;

    public SliderHelper(ViewPager pager, LinearLayout linearLayout, ArrayList<String> urls) {
        mViewPager = pager;
        mLinearLayout = linearLayout;
        mUrls = urls;
    }

    public void initViewPager() {
        mSliderAdapter = new ImageSliderAdapter(mUrls);

        mViewPager.setAdapter(mSliderAdapter);
        mViewPager.setCurrentItem(0);

        addBottomDots(mLinearLayout, mSliderAdapter.getCount(), 0);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(mLinearLayout, mSliderAdapter.getCount(), position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        startAutoSlide(mSliderAdapter.getCount());

    }

    public void startAutoSlide(final int count){
        if (mViewPager != null){
            runnable = new Runnable() {
                @Override
                public void run() {
                    int pos = mViewPager.getCurrentItem();
                    pos = pos +1;
                    if (pos >= count) pos = 0;
                    mViewPager.setCurrentItem(pos);
                    handler.postDelayed(runnable, SLIDE_DELAY);
                }
            };
            handler.postDelayed(runnable, SLIDE_DELAY);
        }
    }

    private void addBottomDots(LinearLayout layout_dots, int size, int current){
        ImageView[] dots = new ImageView[size];

        layout_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++){
            dots[i] = new ImageView(layout_dots.getContext());
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle_outline);
            layout_dots.addView(dots[i]);
        }

        if (dots.length > 0){
            dots[current].setImageResource(R.drawable.shape_circle);
        }
    }
}
