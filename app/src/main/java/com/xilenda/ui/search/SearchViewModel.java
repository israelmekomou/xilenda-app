package com.xilenda.ui.search;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xilenda.ui.product.Product;
import com.xilenda.utils.ApiUtils;
import com.xilenda.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Israel MEKOMOU [09/07/2020].
 */
public class SearchViewModel extends ViewModel {
    private MutableLiveData<String> mError;
    private MutableLiveData<Boolean> mLoad;
    private MutableLiveData<ArrayList<Product>> mProducts;

    public SearchViewModel() {
        mError = new MutableLiveData<>();
        mLoad = new MutableLiveData<>();
        mProducts = new MutableLiveData<>();
    }

    public MutableLiveData<String> getError() {
        return mError;
    }

    public MutableLiveData<Boolean> getLoad() {
        return mLoad;
    }

    public MutableLiveData<ArrayList<Product>> getProducts() {
        return mProducts;
    }

    public void searchProducts(String search){
        mLoad.setValue(true);
        Call<ResponseBody> call = ApiUtils.getCProductsService().searchProduct(search, 100);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                mLoad.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    try {
                        String responseString = response.body().string();
                        JSONArray object = new JSONArray(responseString);
                        ArrayList<Product> products = new Gson().fromJson(object.toString(), new TypeToken<ArrayList<Product>>() {
                        }.getType());
                        mProducts.setValue(products);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        mError.setValue(Utils.API_ERROR_MESSAGE);
                    }
                }else {
                    mError.setValue(Utils.API_ERROR_MESSAGE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                mLoad.setValue(false);
                mError.setValue(Utils.ON_FAILURE_MESSAGE);
            }
        });
    }
}
