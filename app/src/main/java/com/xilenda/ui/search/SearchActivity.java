package com.xilenda.ui.search;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.xilenda.BaseActivity;
import com.xilenda.R;
import com.xilenda.ui.product.Product;
import com.xilenda.ui.product.ProductAdapter;
import com.xilenda.ui.subcategoryProducts.SubcategoryActivity;
import com.xilenda.uiComponents.SpacingItemDecoration;
import com.xilenda.uiComponents.Tools;
import com.xilenda.uiComponents.Utilities;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Created by Israel MEKOMOU [08/07/2020].
 */
public class SearchActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initUi();
    }

    private void initUi() {
        final SearchViewModel viewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        final ImageButton buttonSearch = findViewById(R.id.ibSearch2);
        final RecyclerView rvProducts = findViewById(R.id.recyclerView);
        final AutoCompleteTextView tvSearch = findViewById(R.id.tvSearch);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        final ProductAdapter adapter = new ProductAdapter(this, true);

        rvProducts.setAdapter(adapter);
        rvProducts.setLayoutManager(new GridLayoutManager(this, 2));
        rvProducts.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 8), true));

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(tvSearch.getText())){
                    viewModel.searchProducts(tvSearch.getText().toString());
                }else {
                    Toasty.info(SearchActivity.this, getString(R.string.hint_enter_search_item), Toast.LENGTH_LONG).show();
                }
            }
        });
        viewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null){
                    Utilities.showAlertDialog(SearchActivity.this, R.drawable.ic_warning_24dp, getString(R.string.title_warning), s, true);
                }
            }
        });

        viewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                 progressBar.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                 tvSearch.setEnabled(!aBoolean);
                 buttonSearch.setEnabled(!aBoolean);
                 rvProducts.setVisibility(aBoolean ? View.GONE : View.VISIBLE);
            }
        });

        viewModel.getProducts().observe(this, new Observer<ArrayList<Product>>() {
            @Override
            public void onChanged(ArrayList<Product> products) {
                if (products != null){
                    adapter.setProducts(products);
                    if (products.isEmpty()){
                        Utilities.showAlertDialog(SearchActivity.this, R.drawable.logo, getString(R.string.no_search_item_found), getString(R.string.enter_another_search_item), true);
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        super.initToolbar(this, getString(R.string.app_name), false, false, true);
    }
}