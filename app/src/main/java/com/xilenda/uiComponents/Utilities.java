package com.xilenda.uiComponents;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import com.bumptech.glide.Glide;
import com.xilenda.MainActivity;
import com.xilenda.R;
import com.xilenda.ui.authentication.AuthActivity;

import java.util.Objects;
/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public class Utilities {
    public static void showForwardTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    public static void showBackwardTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_right);
    }

    public static void openActivity(Activity from, String to) {
        try {
            Class<?> c = Class.forName(to);
            Intent intent = new Intent(from, c);
            if (to.equals(MainActivity.class.getName()) || to.equals(AuthActivity.class.getName())){
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            from.startActivity(intent);
            Utilities.showForwardTransition(from);

        } catch (ClassNotFoundException ignored) {
        }
    }

    public static void hideKeyBoard(Activity activity){
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static void showAlertDialog(Activity activity, int icon, String title, String message, boolean cancelable){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setIcon(icon)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        if (!activity.isFinishing())builder.create().show();
    }

    public static void displayImageFromUrl(Context context, String url, ImageView imageView){
        Glide.with(context)
                .load(url)
                .into(imageView);

    }
}
