package com.xilenda.apiServices;

import com.xilenda.ui.authentication.Customer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Israel MEKOMOU [06/07/2020].
 */
public interface AuthenticationApi {

    @Headers({
            "Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("/")
    Call<ResponseBody> register(
            @Query("wpwhpro_action")String wpwhpro_action,
            @Query("wpwhpro_api_key")String wpwhpro_api_key,
            @Field("action")String action,
            @Field("display_name")String name,
            @Field("user_email")String email,
            @Field("user_login")String login,
            @Field("user_pass")String password
    );

    @Headers("Content-Type: application/json")
    @POST("wc/v3/customers")
    Call<ResponseBody> register(
            @Query("consumer_key")String ck,
            @Query("consumer_secret")String cs,
            @Query("email")String email,
            @Query("first_name")String name,
            @Query("username")String username,
            @Query("password")String password
    );

    @Headers("Content-Type: application/json")
    @GET("wc/v3/customers/{id}")
    Call<ResponseBody> getUserInfo(
            @Path("id")int id,
            @Query("consumer_key")String ck,
            @Query("consumer_secret")String cs
    );

    @Headers("Content-Type: application/json")
    @PUT("wc/v3/customers/{id}")
    Call<ResponseBody> editUserInfo(
            @Path("id")int id,
            @Query("consumer_key")String ck,
            @Query("consumer_secret")String cs,
            @Body Customer customer
            );

    @Headers("Content-Type: application/json")
    @POST("/shop/user/generate_auth_cookie/")
    Call<ResponseBody> loginWithEmail(
            @Query("email")String email,
            @Query("password")String password
    );

    @Headers("Content-Type: application/json")
    @POST("/shop/user/generate_auth_cookie/")
    Call<ResponseBody> loginWithUsername(
            @Query("username")String username,
            @Query("password")String password
    );

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("bdpwr/v1/reset-password")
    Call<ResponseBody> requestResetCode(
            @Query("email")String email
    );

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("bdpwr/v1/set-password")
    Call<ResponseBody> resetPassword(
            @Query("email") String email,
            @Query("code") String code,
            @Query("password") String password
    );
}
