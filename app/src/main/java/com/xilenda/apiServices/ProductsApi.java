package com.xilenda.apiServices;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Israel MEKOMOU [07/07/2020].
 */
public interface ProductsApi {
    @GET("wc/v3/products")
    @Headers("Accept: application/json")
    Call<ResponseBody> getProductsFromCategory(
            @Query("category") int  category,
            @Query("per_page") int limit
    );

    @GET("wc/v3/products/{id}")
    @Headers("Accept: application/json")
    Call<ResponseBody> getProduct(
            @Path("id")int id
    );

    @GET("wc-api/v3/products/categories")
    @Headers("Accept: application/json")
    Call<ResponseBody> getAllCategories(
            @Query("consumer_key")String ck,
            @Query("consumer_secret")String sk,
            @Query("filter[limit]") int limit
    );

    @GET("wc/v3/products")
    @Headers("Accept: application/json")
    Call<ResponseBody> searchProduct(
            @Query("search")String search,
            @Query("per_page") int limit

    );
}
