package com.xilenda.apiServices;

import com.xilenda.ui.orderHistory.Order;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Israel MEKOMOU [14/07/2020].
 */
public interface OrderApi {
    @Headers("Content-Type: application/json")
    @POST("wc/v3/orders")
    Call<ResponseBody> create(
            @Body Order order
    );

    @GET("wc/v3/orders")
    @Headers("Accept: application/json")
    Call<ResponseBody> getOrders(
            @Query("customer") int customerId
    );

    @PUT("wc/v3/orders/{id}")
    @Headers("Accept: application/json")
    Call<ResponseBody> cancelOrder(
            @Path("id") int orderId,
            @Query("status") String status
    );

    @GET("wc/v3/shipping_methods")
    @Headers("Accept: application/json")
    Call<ResponseBody> getShippingMethods(
    );
}
